var coordStr = [];
var poly;
var map;
var streetLength;
var jalan, idJenisJalan, panjangRuas, lebarRuas, mEksisting;
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
    }
});

//validate points of road digitizing
function validate() {
    if (coordStr.length == 0) {
        alert("Silahkan Lengkapi Data");
        return false;
    } else {
        return true;
    }
}

$("#submit").on("click", function(response) {
    jalan = $("#nama_jalan").val();
    var ruas = $("#ruas_jalan").val();
    idJenisJalan = $("#id_jenis_jalan").val();
    panjangRuas = $("#panjang_ruas").val();
    lebarRuas = $("#lebar_ruas").val();
    mEksisting = $("#id_eksisting").val();
    console.log(mEksisting);
    if (!validate()) return false;
    $.ajax({
        url: "/digitasi-jalan",
        type: "post",
        data: {
            nama_jalan: jalan,
            ruas_jalan: ruas,
            koordinat: coordStr,
            panjang_ruas: panjangRuas,
            lebar_ruas: lebarRuas,
            id_jenis_jalan: idJenisJalan,
            id_eksisting: mEksisting
        },
        success: function(response) {
            location.href = "/digitasi-jalan";
        },
        error: function(e) {
            // location.href = "/digitasi-jalan/create";
            $("#error-alert").show();
            if (e.status == 422) {
                var msg = "";
                console.log(e.responseJSON.errors);
                $.each(e.responseJSON.errors, function(i, error) {
                    console.log(error[0]);
                    msg = msg + error[0] + "\r\n";
                });
                $("#error_messages").text(msg);
                document
                    .getElementById("error-alert")
                    .scrollIntoView({ behavior: "smooth" });
            }
        }
    });
});

//update mode
if ($("#update").is(":visible")) {
    var id_jalan = $("#id_jalan").val();
    console.log(id_jalan);
    $.ajax({
        url: "/digitasi-jalan/" + id_jalan,
        type: "get",
        success: function(response) {
            coordStr = [];
            console.log(response);
            response.forEach(function(item) {
                var pos = {};
                pos.lat = parseFloat(item.latitude);
                pos.lng = parseFloat(item.longitude);
                coordStr.push(pos);
            });
            // console.log
            setPolyline(coordStr, true);
        },
        error: function(e) {
            console.log(e);
        }
    });
}

$("#update").on("click", function() {
    jalan = $("#nama_jalan").val();
    var ruas = $("#ruas_jalan").val();
    idJenisJalan = $("#id_jenis_jalan").val();
    panjangRuas = $("#panjang_ruas").val();
    lebarRuas = $("#lebar_ruas").val();
    mEksisting = $("#id_eksisting").val();
    if (!validate()) return false;
    console.log(jalan);
    $.ajax({
        url: "/digitasi-jalan",
        type: "post",
        data: {
            nama_jalan: jalan,
            ruas_jalan: ruas,
            koordinat: coordStr,
            isEdit: true,
            id_jalan: $("#id_jalan").val(),
            panjang_ruas: panjangRuas,
            lebar_ruas: lebarRuas,
            id_jenis_jalan: idJenisJalan,
            id_eksisting: mEksisting
        },
        success: function(response) {
            location.href = "/digitasi-jalan";
        },
        error: function(e) {
            $("#error-alert").show();
            if (e.status == 422) {
                var msg = "";
                console.log(e.responseJSON.errors);
                $.each(e.responseJSON.errors, function(i, error) {
                    console.log(error[0]);
                    msg = msg + error[0] + "\r\n";
                });
                $("#error_messages").text(msg);
                document
                    .getElementById("error-alert")
                    .scrollIntoView({ behavior: "smooth" });
            }
        }
    });
});

//csv reader
$("#btnImport").on("click", function() {
    console.log("KESINI");
    if ($("#csvFile").val().length == 0) {
        alert("masukan file csv dulu");
        return false;
    }
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    if (
        regex.test(
            $("#csvFile")
                .val()
                .toLowerCase()
        )
    ) {
        if (typeof FileReader != "undefined") {
            var reader = new FileReader();
            reader.onload = function(e) {
                // console.log(e);
                coordStr = [];
                var coordinates = [];
                var rows = e.target.result.split("\n");
                // console.log(rows);
                for (var i = 1; i < rows.length; i++) {
                    var cells = rows[i].split(",");
                    console.log(cells);
                    if (cells.length > 1) {
                        if (cells[0].length > 0 && cells[1].length > 0) {
                            var coordinate = {};
                            coordinate.lat = parseFloat(cells[0]);
                            coordinate.lng = parseFloat(cells[1]);
                            coordinates.push(coordinate);
                        }
                    }
                }
                // console.log(coordinates);
                setPolyline(coordinates, true);
                coordStr = coordinates;
            };
            reader.readAsText($("#csvFile")[0].files[0]);
            $("#importModal").modal("hide");
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid CSV file.");
    }
    // $('#csvFile').rules('add',{
    //     required: true
    // })
    // return false;
});

//maps
function initMap() {
    var infoWindow, geocoder, marker;
    var input = document.getElementById("search");
    var searchBox = new google.maps.places.SearchBox(input);
    geocoder = new google.maps.Geocoder();
    var markers = [];
    map = new google.maps.Map(document.getElementById("mapKu"), {
        disableDefaultUI: false,
        styles: styles,
        center: { lat: -8.672716, lng: 115.226089 },
        zoom: 13,
        mapTypeControl: true
    });

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP
    });

    infoWindow = new google.maps.InfoWindow();

    //get user current location
    if (navigator.geolocation) {
        if (!$("#update").is(":visible")) {
            navigator.geolocation.getCurrentPosition(
                function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    infoWindow.setPosition(pos);
                    infoWindow.setContent(
                        "<i class='fa fa-user'></i> Posisi anda saat ini"
                    );
                    infoWindow.open(map);
                    map.setZoom(18);
                    map.setCenter(pos);
                },
                function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                }
            );
        }
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    //google maps search libs
    map.addListener("bounds_changed", function() {
        searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener("places_changed", function() {
        var places = searchBox.getPlaces();

        if (places.length === 0) return;

        markers.forEach(function(m) {
            m.setMap(null);
        });
        markers = [];

        var bounds = new google.maps.LatLngBounds();

        places.forEach(function(p) {
            if (!p.geometry) return;

            markers.push(
                new google.maps.Marker({
                    map: map,
                    title: p.name,
                    position: p.geometry.location
                })
            );

            if (p.geometry.viewport) bounds.union(p.geometry.viewport);
            else bounds.extend(p.geometry.location);
        });
        map.fitBounds(bounds);
    });

    //init polyline
    poly = new google.maps.Polyline({
        geodesic: true,
        strokeColor: "#FF0001",
        strokeOpacity: 1.0,
        strokeWeight: 5,
        editable: true,
        draggable: true,
        geodesic: true
    });
    poly.setMap(map);

    //add map click listener
    map.addListener("click", addLatLng);

    //add point to polyline
    function addLatLng(event) {
        var path = poly.getPath();
        // Because path is an MVCArray, we can simply append a new coordinate
        // and it will automatically appear.
        path.push(event.latLng);
        // var posPoly = {
        //     lat: event.latLng.lat(),
        //     lng: event.latLng.lng()
        // }

        // coordStr.push(posPoly);
        // console.log(coordStr);
        // console.log(polys);
    }

    //delete point of polylines
    google.maps.event.addListener(poly, "rightclick", e => {
        if (e.vertex == undefined) {
            return;
        }
        console.log(e.vertex);
        coordStr.splice(e.vertex, 1);
        console.log(coordStr);
        setPolyline(coordStr, false);
    });
    setListener(poly);

    function isMapClickListener(bool) {
        if (bool) {
            map.addListener("click", addLatLng);
        } else {
            google.maps.event.clearListeners(map, "click");
        }
    }
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(
        browserHasGeolocation
            ? "Error: The Geolocation service failed."
            : "Error: Your browser doesn't support geolocation."
    );
    infoWindow.open(map);
}

function setPolyline(coordinates, isFromImport) {
    poly.setPath(coordinates);

    if (isFromImport) {
        map.setCenter(coordinates[0], coordinates[1]);
        map.setZoom(18);
    }
    poly.setMap(map);
    calculateLength(poly);
    setListener(poly);
}

function setListener(poly) {
    google.maps.event.addListener(poly, "dragend", getPath);
    google.maps.event.addListener(poly.getPath(), "insert_at", getPath);
    google.maps.event.addListener(poly.getPath(), "remove_at", getPath);
    google.maps.event.addListener(poly.getPath(), "set_at", getPath);
}

//update polyline
function getPath() {
    var kecamatan, provinsi;
    var path = poly.getPath();
    var len = path.getLength();
    var lastCoord = path.getArray()[len - 1];
    var posPoly = {
        lat: lastCoord.lat(),
        lng: lastCoord.lng()
    };
    var geocoder = new google.maps.Geocoder();
    console.log(posPoly);
    geocoder.geocode(
        {
            latLng: posPoly
        },
        function(results, status) {
            // console.log(status);
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results[0]);
                results[0].address_components.forEach(element => {
                    if (element.types[0] == "administrative_area_level_3") {
                        kecamatan = element.long_name.toLowerCase();
                    }
                    if (element.types[0] == "administrative_area_level_1") {
                        provinsi = element.long_name.toLowerCase();
                    }
                });

                var jenisJalan = $("#jenis_jalan")
                    .val()
                    .toLowerCase();

                if (
                    jenisJalan.includes("kota") ||
                    jenisJalan.includes("kabupaten")
                ) {
                    console.log(
                        provinsi +
                            $("#provinsi")
                                .val()
                                .toLowerCase()
                    );
                    if (
                        !provinsi.includes(
                            $("#provinsi")
                                .val()
                                .toLowerCase()
                        )
                    ) {
                        alert("Jalan anda tidak berada pada provinsi ini");
                        setPolyline(path.getArray().slice(0, len - 1));
                    }
                } else if (jenisJalan.includes("desa")) {
                    console.log(
                        kecamatan +
                            $("#kecamatan")
                                .val()
                                .toLowerCase()
                    );
                    if (
                        !kecamatan.includes(
                            $("#kecamatan")
                                .val()
                                .toLowerCase()
                        )
                    ) {
                        alert("Jalan anda tidak berada pada kecamatan ini");
                        setPolyline(path.getArray().slice(0, len - 1));
                    }
                }
            }
        }
    );

    calculateLength(poly);
    coordStr = [];
    for (var i = 0; i < len; i++) {
        var pos = {
            lat: path.getAt(i).lat(),
            lng: path.getAt(i).lng()
        };
        coordStr.push(pos);
    }
    // console.log(poly.inKm());
    console.log(coordStr);
}

//calculate street length
function calculateLength(poly) {
    google.maps.LatLng.prototype.kmTo = function(a) {
        var e = Math,
            ra = e.PI / 180;
        var b = this.lat() * ra,
            c = a.lat() * ra,
            d = b - c;
        var g = this.lng() * ra - a.lng() * ra;
        var f =
            2 *
            e.asin(
                e.sqrt(
                    e.pow(e.sin(d / 2), 2) +
                        e.cos(b) * e.cos(c) * e.pow(e.sin(g / 2), 2)
                )
            );
        return f * 6378.137;
    };

    google.maps.Polyline.prototype.inM = function(n) {
        var a = this.getPath(n),
            len = a.getLength(),
            dist = 0;
        for (var i = 0; i < len - 1; i++) {
            dist += a.getAt(i).kmTo(a.getAt(i + 1));
        }
        var meter = dist * 100;
        return (Math.round(meter * 100) / 100).toFixed(2);
    };
    streetLength = poly.inM();
    $("#panjang_ruas").val(streetLength);
    console.log("length of street = " + streetLength);
}

var styles = [
    {
        featureType: "poi",
        stylers: [{ visibility: "off" }]
    }
];
