@extends('layouts.layout')
@section('title','Edit Eksisting Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title">Edit Eksisting Jalan</h3>
            </div>
            <div class="card-body">
                <form action="{{route('eksisting.update',['eksisting'=> $eksisting->id])}}" method="POST" class="form-group">
                    @csrf
                    @method("PUT")
                    <div class="form-body">
                        <div class="form-label">
                            <label for="nim">Eksisting Jalan</label>
                        </div>
                        <input type="text" name="eksisting" class="form-control" value="{{$eksisting->eksisting}}">
                
                        <br>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection