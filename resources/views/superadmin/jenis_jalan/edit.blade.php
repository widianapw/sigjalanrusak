@extends('layouts.layout')
@section('title','Edit Jenis Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title">Edit Jenis Jalan</h3>
            </div>
            <div class="card-body">
                <form action="/superadmin/jenis-jalan/{{$jenisJalan->id}}" method="POST" class="form-group">
                    @csrf
                    @method("PUT")
                    <div class="form-body">
                        <div class="form-label">
                            <label for="nim">Jenis Jalan</label>
                        </div>
                        <input type="text" name="jenis_jalan" class="form-control" value="{{$jenisJalan->jenis_jalan}}">
                
                        <br>
                            <button type="submit" class="btn btn-primary">Ubah</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection