@extends('layouts.layout')

@section('title','Tambah Jenis Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-orange">
            <div class="card-header">
                <h3 class="card-title">Jenis Jalan</h3>
            </div>
            <div class="card-body">
                <form action="{{route('jenis-jalan.store')}}" method="POST" class="form-group">
                    @csrf
                    <div class="form-body">
                        <div class="form-label">
                            <label for="nim">Jenis Jalan</label>
                        </div>
                        <input type="text" name="jenis_jalan" class="form-control">
                
                        <br>
                        <div class="form-footer">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection