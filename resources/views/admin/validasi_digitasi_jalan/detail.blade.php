@extends('layouts.layout')
@section('title','Detail Digitasi Jalan')
@section('content')
    <style>
        table, th, td {
            /* border: 1px solid black; */
            border-collapse: collapse;
        }

        th, td {
            padding-right: 15px;
            /* padding-left: 5px; */
            padding-top: 5px;
            padding-bottom: 5px;
        }
    </style>
    <div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="card card-blue">
                    <div class="card-header">
                        <h3 class="card-title"> Detail Digitasi Jalan</h3>
                    </div>

                    <div class="card-body">
                        {{-- <div class="callout callout-info">
                            <h4>Informasi Penambahan Digitasi Jalan</h4>
                            <p>*Dapat menambahkan digitasi jalan dengan import data dalam CSV <a href="{{asset('/file/format_digitasi_jalan.csv')}}">Sesuai Format</a> atau menggambar pada peta<br>
                                *Klik kiri pada peta untuk menambah titik jalan <br>
                                *Klik kanan pada titik jalan untuk menghapus <br>
                            </p><a href="{{asset('/file/format_digitasi_jalan.csv')}}">Download Format CSV Digitasi Jalan</a>

                        </div> --}}
                        <input type="hidden" id="idJalan" value="{{$data->id}}">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control" id="search" placeholder="Cari Jalan...">
                        </div>

                        <div id="mapKu" class="mt-3"></div>
                        <hr>
                        <label for="nama_jalan">Nama Jalan</label>
                        <input type="text" class="form-control" id="nama_jalan" value="{{$data->nama_jalan}}" readonly>
                        <br>

                        <label for="nama_jalan">Ruas Jalan</label>
                        <input type="text" class="form-control" id="nama_jalan" value="{{$data->ruas_jalan}}" readonly>
                        <br>


                        <label for="nama_jalan">Panjang Ruas</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="panjangRuas" value="{{$data->panjang_ruas}}"
                                   readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">Meter</div>
                            </div>
                        </div>

                        <br>
                        <label for="nama_jalan">Lebar Ruas</label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="lebarRuas" value="{{$data->lebar_ruas}}"
                                   readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">Meter</div>
                            </div>
                        </div>

                        <br>
                        <label for="eksisting">Eksisting</label>
                        <input type="text" class="form-control" id="eksisting"
                               value="{{$data->eksistingJalan->eksisting}}" readonly>
                        <br>

                    </div>

                </div>
            </div>
        </div>

        {{-- modal --}}
        <div id="map"></div>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Street Map</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div id="pano" style="height:300px; width:100%;"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('/js/admin_validasi_digitasi_jalan.js')}}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYsDrPWaL32zuvMCQ-8lely3pQ0MWjv4o&libraries=places,geometry&callback=initMap"
        async defer></script>
@endsection