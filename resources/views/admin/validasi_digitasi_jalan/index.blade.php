@extends('layouts.layout')
@section('title','Jenis Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-purple">
            <div class="card-header">
                <h3 class="card-title">Validasi Digitasi Jalan</h3>
            </div>

        <div class="card-body ">
            <div class="table-responsive">
            <table id="table-keuangan" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Pengelola</th>
                        <th>Nama Jalan</th>
                        <th>Ruas Jalan</th>
                        <th>Tanggal Dibuat</th>
                        <th>Status</th>
                        <th>Detail</th>
                        <th>Verifikasi</th>
                        <th>Tolak</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $m)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $m->username }}</td>
                        <td>{{ $m->nama_jalan }}</td>
                        <td>{{$m->ruas_jalan}}</td>
                        <td>@dateFormat($m->created_at)</td>
                        <td>
                            @if ($m->status == '0')
                                Menunggu Verifikasi
                            @elseif($m->status == '1')
                                Terverifikasi
                            @else
                                Ditolak
                            @endif
                        </td>
                        <td>

                            <a href="/admin/validasi-digitasi-jalan/{{$m->id}}">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-fw fa-eye" style="color:white"></i>
                                </button>
                            </a>
                        </td>
                        <td>
                                <center>
                            @if ($m->status != "1")
                            <form action="/admin/validasi-digitasi-jalan/verif/{{$m->id}}" method="POST">
                                @method("PUT")
                                @csrf
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-fw fa-check"></i>
                                </button>
                            </form>
                            @else
                                <i class="fa fa-check green"></i>
                            @endif
                                </center>
                        </td>
                        <td>
                            <center>
                            @if($m->status != "2")
                            <form action="/admin/validasi-digitasi-jalan/tolak/{{$m->id}}" method="POST">
                                @method("PUT")
                                @csrf
                                <button type="submit" class="btn btn-danger">
                                    <i class="fa fa-fw fa-times"></i>
                                </button>
                            </form>
                            @else
                                <i class="fa fa-times red"></i>
                            @endif
                            </center>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('#table-keuangan').DataTable();
        });
    </script>
@endsection
