@extends('layouts.layout')
@section('title','Admin')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="callout callout-info">
            <h4>Selamat Datang {{Auth::user()->username}}!</h4>
            <p>Mulai Harimu dengan senyuman</p>
        </div>
        <div class="card card-purple">
            <div class="card-header">
                Digitasi Jalan
            </div>
            <div class="card-body">
                <div class="callout callout-info">
                    <h4>Total Digitasi Jalan</h4>
                    <h5>{{ $total }}</h5>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Status Digitasi Jalan</th>
                            <th>Jumlah</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Menunggu Verifikasi
                                </td>
                                <td>
                                    {{ $totalMenungguVerifikasi }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Terverifikasi
                                </td>
                                <td>
                                    {{ $totalTerverifikasi }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ditolak
                                </td>
                                <td>
                                    {{ $totalDitolak }}
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>


        {{-- Pengaduan Section --}}
        <div class="card card-green">
            <div class="card-header">
                Pengaduan Jalan
            </div>
            <div class="card-body">
                <div class="callout callout-info">
                    <h4>Total Pengaduan Jalan</h4>
                    <h5>{{ $totalPengaduan }}</h5>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Status Pengaduan Jalan</th>
                            <th>Jumlah</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Menunggu Verifikasi
                                </td>
                                <td>
                                    {{ $totalPengaduanMenunggu }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Terverifikasi
                                </td>
                                <td>
                                    {{ $totalPengaduanTerverifikasi }}
                                </td>
                            </tr>
                            <tr>
                                <td>Terperbaiki</td>
                                <td>{{$totalPengaduanSudahDiperbaiki}}</td>
                            </tr>
                            <tr>
                                <td>
                                    Ditolak
                                </td>
                                <td>
                                    {{ $totalPengaduanDitolak }}
                                </td>
                            </tr>
                        </tbody>
        
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- <audio src="https://cdn.discordapp.com/attachments/757420058581532772/807479346737971200/alesana.mp3" controls loop autoplay>
        <p>If you are reading this, it is because your browser does not support the audio element.</p>
    </audio> --}}
    {{-- <embed src="{{asset('song/alesana.mp3')}}" loop="true" autostart="true" width="2" height="0"> --}}
</div>
@endsection
