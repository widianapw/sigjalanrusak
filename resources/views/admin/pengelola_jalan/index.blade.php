@extends('layouts.layout')
@section('title','Pengelola Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-green">
            <div class="card-header">
                <h3 class="card-title">Pengelola Jalan</h3>
            </div>
        
        <div class="card-body ">
                {{-- <a href="{{route('pengelola-jalan.create')}}">
                    <button class="btn btn-primary mb-4"><i class="fa fa-plus"></i>
                        Tambah Pengelola Jalan
                    </button>
                </a> --}}
            <div class="table-responsive">
            <table id="table-keuangan" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Email</th>
                        <th>Nama</th>
                        <th>Jenis Jalan</th>
                        <th>Wilayah</th>
                        <th>Tanggal</th>
                        <th>Persetujuan</th>
                        {{-- <th>Edit</th>
                        <th>Hapus</th> --}}
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $m)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $m->user->email }}</td>
                        <td>{{ $m->user->username }}</td>
                        <td>{{$m->jenisJalan->jenis_jalan}}</td>
                        @php $jalan = strtolower($m->jenisJalan->jenis_jalan) @endphp
                        @if ( $jalan == "provinsi")
                            <td>{{$m->provinsi->provinsi}}</td>    
                        @elseif($jalan == "kabupaten")
                            <td>{{$m->kabupaten->kabupaten}}</td>    
                        @elseif($jalan == "kota")
                            <td>{{$m->kabupaten->kabupaten}}</td>    
                        @elseif($jalan == "desa")
                            <td>{{$m->desa->desa}}</td>    
                        @endif
                        
                        <td>@dateFormat($m->created_at)</td>
                        <td>
                            @if (is_null($m->verified))
                                <form action="{{route('admin.pengelola-jalan.changeStatus',['pengelola_jalan'=>$m->id])}}" method="POST">
                                    @csrf
                                    @method("PUT")
                                    <button type="input" class="btn btn-primary">Approve</button>
                                </form>    
                            @else
                                Approved
                            @endif

                            
                        </td>
                        
                        {{-- <td>
                            
                                <a href="{{route('pengelola-jalan.edit',['pengelola_jalan'=>$m->id])}}"><button type="submit" class="btn btn-warning">
                                    <i class="fa fa-fw fa-edit" style="color:white"></i>
                                </button></a>
                            
                        </td>
                        <td>
                            <form action="{{route('pengelola-jalan.destroy',['pengelola_jalan'=>$m->id])}}" method="POST">
                                @method("DELETE")
                                @csrf
                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                    <i class="fa fa-fw fa-trash"></i>
                                </button>
                            </form>
                        </td>     --}}
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
    </div>
</div>

@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('#table-keuangan').DataTable();
        });
    </script>
@endsection