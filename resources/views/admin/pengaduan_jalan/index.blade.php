@extends('layouts.layout')
@section('title','Pengaduan Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title">Pengaduan Jalan</h3>
            </div>
            <div class="card-body">
                <div class="table-responsive tab-pane active p-3" id="belum">
                    <table id="table-data" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Jalan</th>
                                <th>Ruas Jalan</th>
                                <th>Nama Pengadu</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Lihat Detail</th>
                                {{-- <th>Beri Komentar</th> --}}
                                {{-- <th>Aksi</th> --}}

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $m)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $m->digitasiJalan->nama_jalan }}</td>
                                <td>{{$m->digitasiJalan->ruas_jalan}}</td>
                                <td>{{ $m->pengguna->user->username }}</td>
                                <td>{{$m->deskripsi}}</td>
                                <td>
                                    {{ \App\PengaduanJalan::getStatusPengaduan($m->status) }}
                                </td>
                                <td>@dateFormat($m->created_at)</td>
                                <td>
                                    <a href="{{route('admin.pengaduan-jalan.show',['pengaduan_jalan'=>$m->id])}}">
                                        <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                    </a>
                                </td>

                                {{-- <td>
                                        <button type="button" class="btn btn-info" data-toggle="modal"
                                                data-target="#komentarModal">
                                            Komentar
                                        </button>
                                    </td> --}}
                                {{-- <td>
                                    <div class="btn-group">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-warning dropdown-toggle"
                                                data-toggle="dropdown">
                                                Aksi
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item"
                                                    href="{{route("pengaduanJalan.changeStatus",["id"=>$m->id])}}"
                                                    onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='1'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Verifikasi</a>
                                                <a class="dropdown-item"
                                                    href="{{route("pengaduanJalan.changeStatus",["id"=>$m->id])}}"
                                                    onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='2'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Terberpaiki</a>
                                                <a class="dropdown-item"
                                                    href="{{route("pengaduanJalan.changeStatus",["id"=>$m->id])}}"
                                                    onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='3'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Tolak</a>

                                                <form id="changeStatusForm-{{ $m->id }}"
                                                    action="{{route("pengaduanJalan.changeStatus",["id"=>$m->id])}}"
                                                    method="POST" style="display: none">
                                                    @csrf
                                                    @method("PUT")
                                                    <input type="hidden" id="status-{{ $m->id }}" name="status"
                                                        value="1">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td> --}}

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                    


                
            </div>
        </div>
    </div>

    <div class="modal fade" id="komentarModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Komentar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form action="#">

                        <input type="text" id="komentar" class="form-control" required name="komentar">
                        <br>
                        <button type="submit" class="btn btn-primary">Kirim Komentar</button>
                    </form>
                </div>

            </div>
        </div>
    </div>


</div>
@endsection
@section('js')
<script>
    $(document).ready(function () {
            $('#table-data').DataTable();
        });
</script>

@endsection