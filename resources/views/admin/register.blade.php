@extends('layouts.app')
@section('content')
<div class="container">
    {{-- <x-slot name="content"> --}}
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-purple">
                <div class="card-header">{{ __('Register Admin') }}</div>

                <div class="card-body">
                    {{-- <x-jet-validation-errors class="mb-4" /> --}}

                    <form method="POST" action="{{ route('admin.register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" >

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" autocomplete="password" >

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ old('password_confirmation') }}" autocomplete="password_confirmation">

                                @error('password_confirmation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="id_jenis_jalan" class="col-md-4 col-form-label text-md-right">{{ __('Jenis Jalan') }}</label>

                            <div class="col-md-6">
                                <select name="id_jenis_jalan" id="id_jenis_jalan" class="form-control @error('id_jenis_jalan') is-invalid @enderror select2" required >
                                    <option value="">Pilih Jenis Jalan</option>
                                    @foreach ($jenisJalan as $item)
                                        <option value="{{$item->id}}">{{$item->jenis_jalan}}</option>
                                    @endforeach
                                </select>
                                @error('id_jenis_jalan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="id_provinsi" style="display:none">
                            <label for="id_provinsi" class="col-md-4 col-form-label text-md-right">{{ __('Provinsi') }}</label>

                            <div class="col-md-6">
                                <select name="id_provinsi" style="width: 100%" id="select_provinsi" class="form-control @error('id_provinsi') is-invalid @enderror select2" >
                                    <option value="">Pilih Provinsi</option>
                                    @foreach ($provinsi as $item)
                                        <option value="{{$item->id}}">{{$item->provinsi}}</option>
                                    @endforeach
                                </select>
                                @error('id_provinsi')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="id_kabupaten" style="display: none">
                            <label for="id_kabupaten" class="col-md-4 col-form-label text-md-right">{{ __('Kabupaten') }}</label>

                            <div class="col-md-6">
                                <select name="id_kabupaten" style="width: 100%" class="form-control @error('id_kabupaten') is-invalid @enderror select2" >
                                    <option value="">Pilih Kabupaten</option>
                                    @foreach ($kabupaten as $item)
                                        <option value="{{$item->id}}">{{$item->kabupaten}}</option>
                                    @endforeach
                                </select>
                                @error('id_kabupaten')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="id_desa" style="display: none">
                            <label for="id_desa" class="col-md-4 col-form-label text-md-right">{{ __('Desa') }}</label>

                            <div class="col-md-6">
                                <select name="id_desa" style="width: 100%" class="form-control @error('id_desa') is-invalid @enderror select2" >
                                    <option value="">Pilih Desa</option>
                                    @foreach ($desa as $item)
                                        <option value="{{$item->id}}">{{$item->desa}}</option>
                                    @endforeach
                                </select>
                                @error('id_desa')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="text-left mb-3 row">
                            <div class="offset-md-4">
                                Sudah Memiliki Akun? <a href="{{route('admin.login')}}">Masuk</a>
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Daftar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- </x-slot> --}}

</div>
<script>
    $("#id_jenis_jalan").on('change', function(){
        var text = $("#id_jenis_jalan option:selected").text().toLowerCase();
        if(text == "provinsi"){
            $("#id_provinsi").show();
            $("#id_kabupaten").hide();
            $("#id_desa").hide();
        }else if(text == "kabupaten"){
            $("#id_provinsi").hide();
            $("#id_kabupaten").show();
            $("#id_desa").hide();
        }else if(text =="kota"){
            $("#id_provinsi").hide();
            $("#id_kabupaten").show();
            $("#id_desa").hide();
        }else if(text == "desa"){
            $("#id_provinsi").hide();
            $("#id_kabupaten").hide();
            $("#id_desa").show();
        }
    });
</script>
@endsection
