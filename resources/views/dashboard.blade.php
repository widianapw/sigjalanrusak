@extends('layouts.layout')
@section('title','Beranda')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">
        <div class="callout callout-info">
            <h4>Selamat Datang {{Auth::user()->username}}!</h4>
            <p>Mulai Harimu dengan senyuman</p>
        </div>
        <div class="card card-purple mt-5">
            <div class="card-header">Pengaduan</div>
            <div class="card-body">
                <div class="callout callout-info">
                    <h4>Total Pengaduan</h4>
                    <p>{{ $totalPengaduan }}</p>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <th>Status Pengaduan</th>
                            <th>Jumlah</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    Menunggu Verifikasi
                                </td>
                                <td>
                                    {{ $totalMenunggu }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Terverifikasi
                                </td>
                                <td>
                                    {{ $totalTerverifikasi }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Terperbaiki
                                </td>
                                <td>
                                    {{ $totalSudahDiperbaiki }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Ditolak
                                </td>
                                <td>
                                    {{ $totalDitolak }}
                                </td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@endsection