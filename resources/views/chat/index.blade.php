
@extends('layouts.layout')
@section('title','Jenis Jalan')
@section('content')
<link rel="stylesheet" href="{{asset('css/message.css')}}">
<div class="row pt-2">
    <div class="col-md-12">
        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title">Chat</h3>
            </div>
        
            <div class="card-body">

                <div class="mesgs">
                    <div class="msg_history pb-5" style="padding-top:-5px">
                        <div class="incoming_msg">
                        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                            <p>Test which is a new approach to have all
                                solutions</p>
                            <span class="time_date"> 11:01 AM    |    June 9</span></div>
                        </div>
                        </div>
                        <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p>Test which is a new approach to have all
                            solutions</p>
                            <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                        </div>
                        <div class="incoming_msg">
                        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                            <p>Test, which is a new approach to have</p>
                            <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                        </div>
                        </div>
                        <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p>Apollo University, Delhi, India Test</p>
                            <span class="time_date"> 11:01 AM    |    Today</span> </div>
                        </div>
                        <div class="incoming_msg">
                        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                            <p>We work directly with our designers and suppliers,
                                and sell direct to you, which means quality, exclusive
                                products, at a price anyone can afford.</p>
                            <span class="time_date"> 11:01 AM    |    Today</span></div>
                            </div>
                        </div>
                        <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p>Test which is a new approach to have all
                            solutions</p>
                            <span class="time_date"> 11:01 AM    |    June 9</span> </div>
                        </div>
                        <div class="incoming_msg">
                        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                            <p>Test, which is a new approach to have</p>
                            <span class="time_date"> 11:01 AM    |    Yesterday</span></div>
                        </div>
                        </div>
                        <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p>Apollo University, Delhi, India Test</p>
                            <span class="time_date"> 11:01 AM    |    Today</span> </div>
                        </div>
                        <div class="incoming_msg">
                        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                            <p>We work directly with our designers and suppliers,
                                and sell direct to you, which means quality, exclusive
                                products, at a price anyone can afford.</p>
                            <span class="time_date"> 11:01 AM    |    Today</span></div>
                        </div>
                        </div>
                    </div>
                    <div class="type_msg">
                        <div class="input_msg_write">
                        <input type="text" class="write_msg px-3" placeholder="Type a message" />
                        <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

            </div>
            {{-- <div class="card-footer">
                <div class="row">
                    <div class="col" style="padding-right:0px">

                        <input type="text" class="form-control">
                    </div>
                    <div class="col-auto" style="padding-left:10px">

                        <button class="text-right btn btn-primary">Kirim</button>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</div>
<style>
    input[type=text]{
        outline:0;
    }
</style>
@endsection