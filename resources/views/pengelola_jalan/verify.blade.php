@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-purple">
                <div class="card-header">Verifikasi</div>

                <div class="card-body">
                    <div class="callout callout-info">
                    <h5><i class="icon fas fa-info"></i> Menunggu Verifikasi</h5>
    
                    <p>akun anda sedang dalam pengecekan admin, mohon tunggu untuk verifikasi.</p>
                    </div>
                    <table class="table table-hover">
                        <tr>
                            <td>Email</td>
                            <td>{{$data->user->email}}</td>
                        </tr>
                        <tr>
                            <td>Username</td>
                            <td>{{$data->user->username}}</td>
                        </tr>
                        <tr>
                            <td>Jenis Jalan</td>
                            <td>{{$data->jenisJalan->jenis_jalan}}</td>
                        </tr>
                        @php $jenis = Str::lower($data->jenisJalan->jenis_jalan) @endphp
                        @if ($jenis == "provinsi")
                            <tr>
                                <td>Provinsi</td>
                                <td>{{$data->provinsi->provinsi}}</td>
                            </tr>    
                        @elseif($jenis == "kabupaten")
                            <tr>
                                <td>Kabupaten</td>
                                <td>{{$data->kabupaten->kabupaten}}</td>
                            </tr>    
                        @elseif($jenis == "kota")    
                            <tr>
                                <td>Kota</td>
                                <td>{{$data->kabupaten->kabupaten}}</td>
                            </tr>    
                        @elseif($jenis == "desa")
                            <tr>
                                <td>Desa</td>
                                <td>{{$data->desa->desa}}</td>
                            </tr>    
                        @endif
                        
                    </table>
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="nav-link">
                        <button class="btn btn-outline-danger">
                            <i class="fas fa-power-off"></i>
                            Ganti Akun
                        </button>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
