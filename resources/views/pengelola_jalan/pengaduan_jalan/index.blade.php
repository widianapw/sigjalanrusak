@extends('layouts.layout')
@section('title', 'Pengaduan Jalan')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">

            <div class="card card-blue">
                <div class="card-header">
                    <h3 class="card-title">Pengaduan Jalan</h3>
                </div>
                <div class="card-body">

                    <ul class="nav nav-tabs" role="tablist" id="tab_custom">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#belum">Belum Diverifikasi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#terverifikasi">Terverifikasi</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#sudah">Sudah Diperbaiki</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#ditolak">Ditolak</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="tab_custom">
                        {{-- belum --}}
                        <div class="table-responsive tab-pane active p-3" id="belum">
                            <table id="table-data" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Jalan</th>
                                        <th>Ruas Jalan</th>
                                        <th>Nama Pengadu</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Lihat Detail</th>
                                        {{-- <th>Beri Komentar</th> --}}
                                        <th>Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $m)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $m->digitasiJalan->nama_jalan }}</td>
                                            <td>{{ $m->digitasiJalan->ruas_jalan }}</td>
                                            <td>{{ $m->pengguna->user->username }}</td>
                                            <td>{{ $m->deskripsi }}</td>
                                            <td>
                                                {{ \App\PengaduanJalan::getStatusPengaduan($m->status) }}

                                            </td>
                                            <td>@dateFormat($m->created_at)</td>
                                            <td>
                                                <a href="/pengaduan-jalan/{{ $m->id }}">
                                                    <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                                </a>
                                            </td>

                                            {{-- <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                    data-target="#komentarModal">
                                                Komentar
                                            </button>
                                        </td> --}}
                                            <td>
                                                <div class="btn-group">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-warning dropdown-toggle"
                                                            data-toggle="dropdown">
                                                            Aksi
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='1'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Verifikasi</a>
                                                            <button id="btnTerperbaiki" type="button" class="dropdown-item"
                                                                data-toggle="modal" data-id="{{ $m->id }}"
                                                                data-target="#modalTerperbaiki">
                                                                Terberpaiki
                                                            </button>
                                                            {{-- <a class="dropdown-item" data-toggle="modal"
                                                                data-target="#exampleModalCenter">Terberpaiki</a> --}}
                                                            {{-- {{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}
                                                                "
                                                                onclick="">Terberpaiki</a> --}}
                                                            {{-- event.preventDefault();
                                                            document.getElementById('status-{{ $m->id }}').value='2';
                                                            document.getElementById('changeStatusForm-{{ $m->id }}').submit()
                                                            ">Terberpaiki</a> --}}
                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='3'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Tolak</a>

                                                            <form id="changeStatusForm-{{ $m->id }}"
                                                                action="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                method="POST" style="display: none">
                                                                @csrf
                                                                @method("PUT")
                                                                <input type="hidden" id="status-{{ $m->id }}"
                                                                    name="status" value="1">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        {{-- terverifikasi --}}
                        <div class="table-responsive tab-pane fade p-3" id="terverifikasi">
                            <table id="table-data-verif" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Jalan</th>
                                        <th>Ruas Jalan</th>
                                        <th>Nama Pengadu</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Lihat Detail</th>
                                        {{-- <th>Beri Komentar</th> --}}
                                        <th>Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dataTerverifikasi as $m)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $m->digitasiJalan->nama_jalan }}</td>
                                            <td>{{ $m->digitasiJalan->ruas_jalan }}</td>
                                            <td>{{ $m->pengguna->user->username }}</td>
                                            <td>{{ $m->deskripsi }}</td>
                                            <td>
                                                {{ \App\PengaduanJalan::getStatusPengaduan($m->status) }}

                                            </td>
                                            <td>@dateFormat($m->created_at)</td>
                                            <td>
                                                <a href="/pengaduan-jalan/{{ $m->id }}">
                                                    <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                                </a>
                                            </td>

                                            {{-- <td>
                                            <button class="btn btn-info">
                                                Komentar
                                            </button>
                                             <form action="{{route('digitasi-jalan.destroy',['digitasi_jalan'=>$m])}}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </form>
                                        </td> --}}
                                            <td>
                                                <div class="btn-group">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-warning dropdown-toggle"
                                                            data-toggle="dropdown">
                                                            Aksi
                                                        </button>
                                                        <div class="dropdown-menu">

                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='0'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Menunggu
                                                                Verifikasi</a>
                                                            <button id="btnTerperbaiki" type="button" class="dropdown-item"
                                                                data-toggle="modal" data-id="{{ $m->id }}"
                                                                data-target="#modalTerperbaiki">
                                                                Terberpaiki
                                                            </button>
                                                            {{-- <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='2'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Terberpaiki</a> --}}
                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='3'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Tolak</a>

                                                            <form id="changeStatusForm-{{ $m->id }}"
                                                                action="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                method="POST" style="display: none">
                                                                @csrf
                                                                @method("PUT")
                                                                <input type="hidden" id="status-{{ $m->id }}"
                                                                    name="status" value="1">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <a href="/digitasi-jalan/{{$m->id}}/edit"><button type="submit" class="btn btn-warning">
                                                <i class="fa fa-fw fa-edit" style="color:white"></i>
                                            </button></a> --}}
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        {{-- Terperbaiki --}}
                        <div class="table-responsive tab-pane fade p-3" id="sudah">
                            <table id="table-data-sudah" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Jalan</th>
                                        <th>Ruas Jalan</th>
                                        <th>Nama Pengadu</th>

                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Lihat Detail</th>
                                        {{-- <th>Beri Komentar</th> --}}
                                        <th>Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dataSudahDiperbaiki as $m)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $m->digitasiJalan->nama_jalan }}</td>
                                            <td>{{ $m->digitasiJalan->ruas_jalan }}</td>
                                            <td>{{ $m->pengguna->user->username }}</td>
                                            <td>{{ $m->deskripsi }}</td>
                                            <td>
                                                {{ \App\PengaduanJalan::getStatusPengaduan($m->status) }}

                                            </td>
                                            <td>@dateFormat($m->created_at)</td>
                                            <td>
                                                <a href="/pengaduan-jalan/{{ $m->id }}">
                                                    <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                                </a>
                                            </td>

                                            {{-- <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                    data-target="#komentarModal">
                                                Komentar
                                            </button>
                                            <form action="{{route('digitasi-jalan.destroy',['digitasi_jalan'=>$m])}}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </form> 
                                        </td> --}}
                                            <td>
                                                <div class="btn-group">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-warning dropdown-toggle"
                                                            data-toggle="dropdown">
                                                            Aksi
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='0'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Menunggu
                                                                Verifikasi</a>

                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='1'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Verifikasi</a>

                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='3'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Tolak</a>

                                                            <form id="changeStatusForm-{{ $m->id }}"
                                                                action="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                method="POST" style="display: none">
                                                                @csrf
                                                                @method("PUT")
                                                                <input type="hidden" id="status-{{ $m->id }}"
                                                                    name="status" value="1">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <a href="/digitasi-jalan/{{$m->id}}/edit"><button type="submit" class="btn btn-warning">
                                                <i class="fa fa-fw fa-edit" style="color:white"></i>
                                            </button></a> --}}
                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        {{-- ditolak --}}
                        <div class="table-responsive tab-pane fade p-3" id="ditolak">
                            <table id="table-data-ditolak" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Jalan</th>
                                        <th>Ruas Jalan</th>
                                        <th>Nama Pengadu</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Tanggal</th>
                                        <th>Lihat Detail</th>
                                        {{-- <th>Beri Komentar</th> --}}
                                        <th>Aksi</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dataDitolak as $m)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $m->digitasiJalan->nama_jalan }}</td>
                                            <td>{{ $m->digitasiJalan->ruas_jalan }}</td>
                                            <td>{{ $m->pengguna->user->username }}</td>
                                            <td>{{ $m->deskripsi }}</td>
                                            <td>
                                                {{ \App\PengaduanJalan::getStatusPengaduan($m->status) }}

                                            </td>
                                            <td>@dateFormat($m->created_at)</td>
                                            <td>
                                                <a href="/pengaduan-jalan/{{ $m->id }}">
                                                    <button class="btn btn-primary"><i class="fa fa-eye"></i></button>
                                                </a>
                                            </td>

                                            {{-- <td>
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                    data-target="#komentarModal">
                                                Komentar
                                            </button>
                                            <form action="{{route('digitasi-jalan.destroy',['digitasi_jalan'=>$m])}}" method="POST">
                                                @method("DELETE")
                                                @csrf
                                                <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                                    <i class="fa fa-fw fa-times"></i>
                                                </button>
                                            </form>
                                        </td> --}}
                                            <td>
                                                <div class="btn-group">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-warning dropdown-toggle"
                                                            data-toggle="dropdown">
                                                            Aksi
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='0'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Menunggu
                                                                Verifikasi</a>


                                                            <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='1'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Verifikasi</a>
                                                            <button id="btnTerperbaiki" type="button" class="dropdown-item"
                                                                data-toggle="modal" data-id="{{ $m->id }}"
                                                                data-target="#modalTerperbaiki">
                                                                Terberpaiki
                                                            </button>
                                                            {{-- <a class="dropdown-item"
                                                                href="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                onclick="event.preventDefault(); document.getElementById('status-{{ $m->id }}').value='2'; document.getElementById('changeStatusForm-{{ $m->id }}').submit() ">Terberpaiki</a> --}}


                                                            <form id="changeStatusForm-{{ $m->id }}"
                                                                action="{{ route('pengaduanJalan.changeStatus', ['id' => $m->id]) }}"
                                                                method="POST" style="display: none">
                                                                @csrf
                                                                @method("PUT")
                                                                <input type="hidden" id="status-{{ $m->id }}"
                                                                    name="status" value="1">
                                                            </form>
                                                        </div>
                                                        {{-- <div class="dropdown-menu"> --}}
                                                        {{-- <a class="dropdown-item" href="#">Tolak</a> --}}
                                                        {{-- </div> --}}
                                                    </div>
                                                </div>
                                                {{-- <a href="/digitasi-jalan/{{$m->id}}/edit"><button type="submit" class="btn btn-warning">
                                                <i class="fa fa-fw fa-edit" style="color:white"></i>
                                            </button></a> --}}
                                            </td>

                                        </tr>


                                    @endforeach
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalTerperbaiki" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Terperbaiki</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="POST" enctype="multipart/form-data"
                        action="{{ route('pengaduanJalan.addImageTerperbaiki') }}">
                        <div class="modal-body">

                            @csrf
                            <input type="hidden" id="idTerperbaiki" name="id_pengaduan">
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Foto jalan terperbaiki</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1"
                                    accept="image/x-png,image/gif,image/jpeg" name="foto_terperbaiki[]" multiple="multiple">
                            </div>

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="komentarModal">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Komentar</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form action="#">

                            <input type="text" id="komentar" class="form-control" required name="komentar">
                            <br>
                            <button type="submit" class="btn btn-primary">Kirim Komentar</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>


    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            $('#table-data').DataTable();
            $('#table-data-verif').DataTable();
            $('#table-data-sudah').DataTable();
            $('#table-data-ditolak').DataTable();

            $("#btnTerperbaiki").click(function() {
                var myBookId = $(this).data('id');
                $('#idTerperbaiki').val($(this).data('id'));
            });
        });
    </script>

@endsection
