@extends('layouts.layout')
@section('title', 'Tambah Digitasi Jalan')
@section('content')
    <div>
        <div class="row pt-2">
            <div class="col-md-12">
                <div class="card card-blue">
                    <div class="card-header">
                        <h3 class="card-title">
                            @if (!empty($digitasiJalan))Ubah @else Tambah @endif Digitasi Jalan
                        </h3>
                    </div>
                    <div class="alert alert-danger alert-dismissible m-3" id="error-alert" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <h5><i class="icon fas fa-times"></i> Validation!</h5>
                        <div style="white-space: pre-line" id="error_messages">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="callout callout-info">
                            <h4>Informasi Penambahan Digitasi Jalan</h4>
                            <p>*Dapat menambahkan digitasi jalan dengan import data dalam CSV <a
                                    href="{{ asset('/file/format_digitasi_jalan.csv') }}">Sesuai Format</a> atau
                                menggambar pada peta<br>
                                *Klik kiri pada peta untuk menambah titik jalan <br>
                                *Klik kanan pada titik jalan untuk menghapus <br>
                            </p><a href="{{ asset('/file/format_digitasi_jalan.csv') }}">Download Format CSV Digitasi
                                Jalan</a>

                        </div>
                        <div class="d-flex justify-content-start">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importModal"
                                data-backdrop="static" data-keyboard="false">+ Import dari CSV
                            </button>
                        </div>
                        <div id="dvCSV">
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" id="search" placeholder="Cari Jalan...">
                            </div>

                            <div id="mapKu" class="mt-3"></div>
                            <hr>
                            @if (!empty($digitasiJalan))
                                <input type="hidden" id="id_jalan" value="{{ $digitasiJalan->id }}">
                            @endif
                            <label for="nama_jalan">Jenis Jalan</label>
                            <input type="hidden" class="form-control" id="id_jenis_jalan"
                                value="{{ $user->jenisJalan->id }}">
                            <input type="text" class="form-control" id="jenis_jalan"
                                value="{{ $user->jenisJalan->jenis_jalan }}" readonly>
                            @php
                                $jenisJalan = strtolower($user->jenisJalan->jenis_jalan);
                            @endphp
                            @if ($jenisJalan == 'kabupaten' || $jenisJalan == 'kota')
                                <input type="hidden" class="form-control" id="provinsi"
                                    value="{{ $validasi->provinsi->provinsi }}" readonly>
                            @elseif($jenisJalan == "desa")
                                <input type="hidden" class="form-control" id="kecamatan"
                                    value="{{ $validasi->kecamatan->kecamatan }}" readonly>
                            @endif
                            <br>
                            <label for="nama_jalan">Nama Jalan</label>
                            <input type="text" class="form-control" id="nama_jalan" @if (!empty($digitasiJalan)) value="{{ $digitasiJalan->nama_jalan }}" @endif>
                            <br>
                            <label for="ruas_jalan">Ruas Jalan</label>
                            <input type="text" class="form-control" id="ruas_jalan" @if (!empty($digitasiJalan)) value="{{ $digitasiJalan->ruas_jalan }}" @endif>
                            <br>
                            <label for="nama_jalan">Panjang Ruas</label>
                            <div class="input-group">
                                <input type="number" step="any" class="form-control" id="panjang_ruas" @if (!empty($digitasiJalan)) value="{{ $digitasiJalan->panjang_ruas }}" @endif
                                    readonly>
                                <div class="input-group-append">
                                    <div class="input-group-text">Meter</div>
                                </div>
                            </div>
                            <br>
                            <label for="nama_jalan">Lebar Ruas</label>
                            <div class="input-group">
                                <input type="number" step="any" class="form-control" id="lebar_ruas" @if (!empty($digitasiJalan)) value="{{ $digitasiJalan->lebar_ruas }}" @endif>
                                <div class="input-group-append">
                                    <div class="input-group-text">Meter</div>
                                </div>
                            </div>
                            <br>
                            <label for="eksisting">Eksisting Jalan</label>
                            <select name="id_eksisting" class="form-control" id="id_eksisting">
                                <option value="">Pilih Eksisting Jalan</option>
                                @foreach ($eksisting as $e)
                                    @if (!empty($digitasiJalan))
                                        @if ($e->id == $digitasiJalan->id_eksisting)
                                            <option value="{{ $e->id }}" selected>{{ $e->eksisting }}</option>
                                        @else
                                            <option value="{{ $e->id }}">{{ $e->eksisting }}</option>
                                        @endif
                                    @else
                                        <option value="{{ $e->id }}">{{ $e->eksisting }}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>
                            @if (!empty($digitasiJalan))
                                <button type="submit" id="update" class="btn btn-warning">Ubah</button>
                            @else
                                <button type="submit" id="submit" class="btn btn-success">Tambah</button>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal fade" id="importModal">
                <div class="modal-dialog modal-dialog-centered" ">
                                <div class=" modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Import CSV</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">

                        <input type="file" id="csvFile" class="form-control" accept=".csv">
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" id="btnImport" class="btn btn-success">Import</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('/js/digitasi-jalan.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYsDrPWaL32zuvMCQ-8lely3pQ0MWjv4o&libraries=places,geometry&callback=initMap"
        async defer></script>
@endsection
