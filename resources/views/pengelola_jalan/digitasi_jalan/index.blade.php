@extends('layouts.layout')
@section('title','Digitasi Jalan')
@section('content')
<div class="row pt-2">
    <div class="col-md-12">

        <div class="card card-blue">
            <div class="card-header">
                <h3 class="card-title">Digitasi Jalan yang anda Kelola</h3>
            </div>
            <div class="card-body">
                <a href="/digitasi-jalan/create">
                    <button class="btn btn-primary mb-4"><i class="fa fa-plus"></i>
                        Tambah Data Jalan
                    </button>
                </a>


                <div class="table-responsive">
                    <table id="table-data" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama</th>
                                <th>Ruas Jalan</th>
                                <th>Panjang Ruas</th>
                                <th>Lebar Ruas</th>
                                <th>Status</th>
                                <th>Tanggal</th>
                                <th>Edit</th>
                                <th>Delete</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $m)

                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $m->nama_jalan }}</td>
                                <td>{{$m->ruas_jalan}}</td>
                                <td>{{$m->panjang_ruas}} Meter</td>
                                <td>{{$m->lebar_ruas}} Meter</td>
                                <td>{{ \App\DigitasiJalan::getJalanStatus($m->status) }}</td>
                                <td>@dateFormat($m->created_at)</td>

                                <td>
                                    <a href="/digitasi-jalan/{{$m->id}}/edit"><button type="submit" class="btn btn-warning">
                                        <i class="fa fa-fw fa-edit" style="color:white"></i>
                                    </button></a>
                                </td>
                                <td>
                                <form action="{{route('digitasi-jalan.destroy',['digitasi_jalan'=>$m])}}" method="POST">
                                        @method("DELETE")
                                        @csrf
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger">
                                            <i class="fa fa-fw fa-trash"></i>
                                        </button>
                                    </form>
                                </td>


                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $('#table-data').DataTable();
    });
</script>

@endsection
