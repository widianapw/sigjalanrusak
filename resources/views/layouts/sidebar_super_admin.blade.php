<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item" class="nav-link">
            <a href="/superadmin" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>Beranda</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="fa fa-users nav-icon purple"></i>
                <p>Admin</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="/superadmin/jenis-jalan" class="nav-link">
                <i class="fab fa-hive nav-icon teal"></i>
                <p>Jenis Jalan</p>
            </a>
        </li>


        <li class="nav-item">
            <a href="/superadmin/eksisting" class="nav-link">
                <i class="nav-icon fa fa-road orange"></i>
                <p>
                    Eksisting Jalan
                </p>
            </a>
        </li>

        {{-- <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fa fa-comments blue"></i>
                <p>
                   Chat Pengelola Jalan 
                </p>
            </a>
        </li> --}}

        <hr>
        <li class="nav-item">
            <a href="{{ route('superadmin.logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" class="nav-link">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>
                    Keluar
                </p>
            </a>
            <form id="logout-form" action="{{ route('superadmin.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>    
        </li>

    </ul>
</nav>