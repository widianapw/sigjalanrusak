<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item" class="nav-link">
            <a href="/admin" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>Beranda</p>
            </a>
        </li>
{{-- 
        <li class="nav-item">
            <a href="/admin/jenis-jalan" class="nav-link">
                <i class="fab fa-hive nav-icon teal"></i>
                <p>Jenis Jalan</p>
            </a>
        </li> --}}

        <li class="nav-item">
            <a href="{{route('admin.pengelola-jalan.index')}}" class="nav-link">
                <i class="nav-icon fa fa-users purple"></i>
                <p>
                    Pengelola Jalan
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{route('admin.validasi-digitasi-jalan.index')}}" class="nav-link">
                <i class="nav-icon fa fa-road orange"></i>
                <p>
                    Validasi Digitasi Jalan
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{route('admin.pengaduan-jalan.index')}}" class="nav-link">
                <i class="fas fa-book nav-icon teal"></i>
                <p>Pengaduan Jalan</p>
            </a>
        </li>

        {{-- <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fa fa-comments blue"></i>
                <p>
                   Chat Pengelola Jalan 
                </p>
            </a>
        </li> --}}

        <hr>
        <li class="nav-item">
            <a href="{{ route('admin.logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" class="nav-link">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>
                    Keluar
                </p>
            </a>
            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>    
        </li>

    </ul>
</nav>