<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
with font-awesome or any other icon font library -->
        <li class="nav-item" class="nav-link">
            <a href="/" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>Beranda</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="/pengaduan-jalan" class="nav-link">
                <i class="fas fa-book nav-icon teal"></i>
                <p>Manajemen Pengaduan</p>
            </a>
        </li>

        <li class="nav-item">
            <a href={{route('digitasi-jalan.index')}} class="nav-link">
                <i class="nav-icon fa fa-road orange"></i>
                <p>
                    Digitasi Jalan
                </p>
            </a>
        </li>

        {{-- <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fa fa-comments blue"></i>
                <p>
                   Chat Admin 
                </p>
            </a>
        </li> --}}
        <hr>
        <li class="nav-item">
            <a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" class="nav-link">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>
                    Keluar
                </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>    
        </li>

    </ul>
</nav>