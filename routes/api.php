<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::put('/pengaduan/changeStatusPengaduan/{pengaduan_jalan}', 'API\PengaduanController@changeStatus');

Route::group(['middleware' => 'auth:api'], function () {
    Route::resource('users', 'API\NuxtController');
    Route::get('getProfile', 'API\UserController@getProfile');
    Route::post('updateProfile', 'API\UserController@updateProfile');
    Route::post('updateProfileImage', 'API\UserController@updateProfileImage');
    Route::post('updateFcmToken', 'API\UserController@updateFcmToken');
    Route::group(['prefix' => 'digitasi'], function () {
        Route::get('getDigitasi', 'API\DigitasiJalanController@index');
    });
    Route::group(['prefix' => 'pengaduan'], function () {
        Route::get('getPengaduan', 'API\PengaduanController@getPengaduan');
        Route::get('getPengaduanSearch/{key}', 'API\PengaduanController@getPengaduanSearch');
        Route::get('getPengaduanDetail/{id}', 'API\PengaduanController@getPengaduanDetail');
        Route::get('getPengaduanTerverifikasi', 'API\PengaduanController@getPengaduanTerverifikasi');
        Route::post('postPengaduan', 'API\PengaduanController@postPengaduan');
        Route::post('uploadImagePengaduan/{id}', 'API\PengaduanController@uploadImagePengaduan');
    });
    Route::group(['prefix' => 'notification'], function () {
        Route::get('getNotification', 'API\PengaduanLogController@index');
    });
});
