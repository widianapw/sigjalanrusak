<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::domain('{username}.final.widianapw.com')->group(function () {
    Route::get('/', 'ChatController@subdomain');
});
Route::group(['namespace' => 'AuthAdmin', 'prefix' => 'admin'], function () {
    Route::get('login', 'AdminAuthController@showLoginForm')->name('admin.login');
    Route::post('login', 'AdminAuthController@login')->name('admin.postlogin');
    Route::post('logout', 'AdminAuthController@logout')->name('admin.logout');
    Route::get('register', 'AdminAuthController@showRegisterForm')->name('admin.register');
    Route::post('register', 'AdminAuthController@register')->name('admin.postRegister');
});

Route::group(['namespace' => 'AuthSuperAdmin', 'prefix' => 'superadmin'], function () {
    Route::get('login', 'SuperAdminAuthController@showLoginForm')->name('superadmin.login');
    Route::post('login', 'SuperAdminAuthController@login')->name('superadmin.postlogin');
    Route::post('logout', 'SuperAdminAuthController@logout')->name('superadmin.logout');
});


Route::group(['middleware' => ['pengelola_jalan']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('digitasi-jalan', 'DigitasiJalanController');
    Route::post('/saveDigitasi', 'DigitasiJalanController@saveDigitasi');
    Route::resource('/pengaduan-jalan', 'PengaduanJalanController');
    Route::put('/pengaduan-jalan/change-status/{id}', 'PengaduanJalanController@changeStatus')->name('pengaduanJalan.changeStatus');
    Route::post('/pengaduan-jalan/add-image-terperbaiki', 'PengaduanJalanController@addTerperbaikiImages')->name('pengaduanJalan.addImageTerperbaiki');
});
Route::get('/verify-pengelola', 'HomeController@verifyPengelola')->name('verifypengelola')->middleware('verifiedPengelolaJalan');

Route::group(['middleware' => ['superadmin'], 'prefix' => 'superadmin'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('eksisting', 'EksistingController');
    Route::resource('jenis-jalan', 'JenisJalanController');
    // Route::post('/saveDigitasi','DigitasiJalanController@saveDigitasi');
    // Route::resource('/pengaduan-jalan','PengaduanJalanController');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('/', 'AdminController');
    // Route::resource('jenis-jalan', 'JenisJalanController');
    Route::resource('pengaduan-jalan', 'AdminPengaduanJalanController');
    Route::resource('validasi-digitasi-jalan', 'ValidasiDigitasiJalanController');
    Route::put('validasi-digitasi-jalan/verif/{id}', 'ValidasiDigitasiJalanController@verifDigitasi')->name('validasi-digitasi-jalan.verif');
    Route::put('validasi-digitasi-jalan/tolak/{id}', 'ValidasiDigitasiJalanController@tolakDigitasi')->name('validasi-digitasi-jalan.tolak');
    Route::get('validasi-digitasi-jalan/detail/{id}', 'ValidasiDigitasiJalanController@detailJalan');
    Route::resource('pengelola-jalan', 'PengelolaJalanController');
    Route::put('pengelola-jalan/verify/{pengelola_jalan}', 'PengelolaJalanController@changeStatus')->name('pengelola-jalan.changeStatus');
});


Route::get('/chat', 'ChatController@index');

Route::get('/pusher', function () {
    return view('pusher');
});

Route::get('/telegramBot', 'ChatController@telegeramBot');
