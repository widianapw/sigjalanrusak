<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pengguna extends Model
{
    use SoftDeletes;

    protected $table = "penggunas";
    protected $guarded = [];
    public $with = ['user', 'pengaduanLog'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function pengaduanLog(){
        return $this->hasMany(PengaduanLog::class, 'id_pengguna');
    }
}
