<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table = "kabupatens";
    protected $guarded = [];

    public function provinsi(){
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }
}
