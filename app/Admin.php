<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;
    protected $table = "admins";
    protected $primaryKey = "id";
    protected $fillable = ["id_user", "wilayah"];
    // protected $with = ['jenis_jalan'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'id_kabupaten');
    }

    public function desa()
    {
        return $this->belongsTo(Desa::class, 'id_desa');
    }

    public function jenisJalan()
    {
        return $this->belongsTo(JenisJalan::class, 'id_jenis_jalan');
    }
}
