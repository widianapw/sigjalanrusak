<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengaduanImage extends Model
{
    use SoftDeletes;
    protected $table = "pengaduan_images";
    protected $guarded = [];
}
