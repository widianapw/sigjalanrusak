<?php

namespace App;

use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengaduanJalan extends Model
{
    use SoftDeletes, Notifiable;

    protected $table = "pengaduan_jalans";
    protected $fillable = [
        "id_digitasi_jalan",
        "id_user",
        "foto_url",
        "deskripsi",
        "status"
    ];

    public $with = ['pengguna', 'digitasiJalan', 'pengaduanLog', 'images'];

    public function digitasiJalan()
    {
        return $this->belongsTo(DigitasiJalan::class, 'id_digitasi_jalan');
    }

    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class, 'id_pengguna');
    }

    public function pengaduanLog()
    {
        return $this->hasMany(PengaduanLog::class, 'id_pengaduan');
    }

    public function images()
    {
        return $this->hasMany(PengaduanImage::class, 'id_pengaduan');
    }

    public static function getStatusPengaduan($key)
    {
        if ($key == "0") {
            return "Menunggu Verifikasi";
        } else if ($key == "1") {
            return "Terverifikasi";
        } else if ($key == "2") {
            return "Terperbaiki";
        } else {
            return "Ditolak";
        }
    }
}
