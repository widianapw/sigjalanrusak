<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    protected $table = "kecamatans";
    protected $guarded = [];

    public function kabupaten(){
        return $this->belongsTo(Kabupaten::class, 'id_kabupaten');
    }
}
