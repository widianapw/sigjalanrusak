<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DigitasiJalan extends Model
{
    use SoftDeletes;
    protected $table = "digitasi_jalans";
    protected $primaryKey = "id";
    protected $guarded = [];
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($digitasiJalans) {
            foreach ($digitasiJalans->pengaduan()->get() as $item) {
                $item->delete();
            }
        });
    }
    public $with = ['jenisjalan', 'eksistingJalan', 'pengelolaJalan', 'detailDigitasi'];
    public function detailDigitasi()
    {
        return $this->hasMany(KoordinatDigitasiJalan::class, 'id_digitasi_jalan');
    }
    public function jenisJalan()
    {
        return $this->belongsTo(JenisJalan::class, 'id_jenis_jalan');
    }

    public function eksistingJalan()
    {
        return $this->belongsTo(Eksisting::class, 'id_eksisting');
    }

    public function pengelolaJalan()
    {
        return $this->belongsTo(PengelolaJalan::class, 'id_pengelola_jalan');
    }

    public function pengaduan()
    {
        return $this->hasMany(PengaduanJalan::class, 'id_digitasi_jalan');
    }


    public static function getJalanStatus($key)
    {
        if ($key == "0") {
            return "Menunggu Verifikasi";
        } else if ($key == "1") {
            return "Terverifikasi";
        } else {
            return "Ditolak";
        }
    }

    public const VALIDATION_RULES = [
        "id_jenis_jalan" => "required",
        "nama_jalan" => "required|min:5",
        "ruas_jalan" => "required|min:5",
        "panjang_ruas" => "required|numeric|min:0.1",
        "lebar_ruas" => "required|numeric|min:0.1",
        "id_eksisting" => "required"
    ];
}
