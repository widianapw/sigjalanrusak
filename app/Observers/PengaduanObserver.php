<?php

namespace App\Observers;
use App\PengaduanJalan;
use App\PushNotification\PengaduanStatus\PengaduanStatusNotificationFactory;
use App\PengaduanLog;
class PengaduanObserver
{
    /**
     * Handle the pengaduan jalan "created" event.
     *
     * @param \App\PengaduanJalan $pengaduanJalan
     * @return void
     */
    public function created(PengaduanJalan $pengaduanJalan)
    {
        //
    }

    /**
     * Handle the pengaduan jalan "updated" event.
     *
     * @param \App\PengaduanJalan $pengaduanJalan
     * @return void
     */
    public function updated(PengaduanJalan $pengaduanJalan)
    {
        $this->insertLog($pengaduanJalan);
        $this->sendPushNotification($pengaduanJalan);
    }

//
//    public function updating(PengaduanJalan $pengaduanJalan)
//    {
//        $this->sendPushNotification($pengaduanJalan);
//    }


    /**
     * Handle the pengaduan jalan "deleted" event.
     *
     * @param \App\PengaduanJalan $pengaduanJalan
     * @return void
     */
    public function deleted(PengaduanJalan $pengaduanJalan)
    {
        //
    }

    /**
     * Handle the pengaduan jalan "restored" event.
     *
     * @param \App\PengaduanJalan $pengaduanJalan
     * @return void
     */
    public function restored(PengaduanJalan $pengaduanJalan)
    {
        //
    }

    /**
     * Handle the pengaduan jalan "force deleted" event.
     *
     * @param \App\PengaduanJalan $pengaduanJalan
     * @return void
     */
    public function forceDeleted(PengaduanJalan $pengaduanJalan)
    {
        //
    }

    public function sendPushNotification($pengaduanJalan)
    {
        if ($pengaduanJalan->isDirty('status')) {
            $notif = PengaduanStatusNotificationFactory::create($pengaduanJalan->status);
            $notif->send($pengaduanJalan);
        }
    }

    public function insertLog($pengaduanJalan){
        if($pengaduanJalan->id && $pengaduanJalan->isDirty('status')){

            if($pengaduanJalan->status == 0){
                $title = "Pengaduan Menunggu Verifikasi";
                $message = "Pengaduan dengan id ". $pengaduanJalan->id ." menunggu verifikasi";
            }else if($pengaduanJalan->status == 1){
                $title = "Pengaduan Terverifikasi";
                $message = "Pengaduan dengan id ". $pengaduanJalan->id ." terverifikasi";
            }else if($pengaduanJalan->status == 2){
                $title = "Pengaduan Terperbaiki";
                $message = "Pengaduan dengan id ". $pengaduanJalan->id ." terperbaiki";
            }else if($pengaduanJalan->status == 3){
                $title = "Pengaduan Selesai";
                $message = "Pengaduan dengan id ". $pengaduanJalan->id ." selesai";
            }else{
                $title = "Pengaduan Ditolak";
                $message = "Pengaduan dengan id ". $pengaduanJalan->id ." ditolak";
            }
            
            PengaduanLog::create([
                'id_pengaduan' => $pengaduanJalan->id,
                'id_pengguna' => $pengaduanJalan->id_pengguna,
                'judul' => $title,
                'deskripsi' => $message
            ]);
        }
    }
}