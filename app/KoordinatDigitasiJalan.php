<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class KoordinatDigitasiJalan extends Model
{
    use SoftDeletes;
    protected $table = "koordinat_digitasi_jalans";
    protected $primaryKey = "id";
    protected $fillable=["id_digitasi_jalan","latitude","longitude"]; 
}
