<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengelolaJalan extends Model
{
    use SoftDeletes;

    protected $table = "pengelola_jalans";
    protected $guarded = [];

    protected $with= ['user','provinsi','kabupaten','desa'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function provinsi()
    {
        return $this->belongsTo(Provinsi::class, 'id_provinsi');
    }

    public function kabupaten()
    {
        return $this->belongsTo(Kabupaten::class, 'id_kabupaten');
    }

    public function desa()
    {
        return $this->belongsTo(Desa::class, 'id_desa');
    }

    public function jenisJalan()
    {
        return $this->belongsTo(JenisJalan::class, 'id_jenis_jalan');
    }
}
