<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
    protected $table = "desas";
    protected $guarded = [];

    public function kecamatan(){
        return $this->belongsTo(Kecamatan::class,'id_kecamatan');
    }
}
