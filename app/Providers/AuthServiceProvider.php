<?php

namespace App\Providers;

use App\DigitasiJalan;
use App\Policies\DigitasiJalanPolicy;
use App\Policies\ValidasiDigitasiJalanPolicy;
use App\Policies\WidianaPolicy;
use App\Widiana;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//         'App\DigitasiJalan' => 'App\Policies\DigitasiJalanPolicy',
        Widiana::class => WidianaPolicy::class,

//        DigitasiJalan::class => DigitasiJalanPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        //
    }
}
