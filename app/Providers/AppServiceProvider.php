<?php

namespace App\Providers;

use App\Observers\PengaduanObserver;
use App\PengaduanJalan;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('dateFormat', function($expression){
            return "<?php echo date('l, d M Y',strtotime($expression)); ?>";
        });
        PengaduanJalan::observe(PengaduanObserver::class);
    }
}
