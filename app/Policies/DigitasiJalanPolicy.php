<?php

namespace App\Policies;

use App\PengelolaJalan;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\DigitasiJalan;
use App\User;

class DigitasiJalanPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\DigitasiJalan  $digitasiJalan
     * @return mixed
     */
    public function view(User $user, DigitasiJalan $digitasiJalan)
    {
        return true;
//        $pengelolaJalan = PengelolaJalan::where('id_user',$user->id)->first();
//        return $pengelolaJalan->id === $digitasiJalan->id_pengelola_jalan;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\DigitasiJalan  $digitasiJalan
     * @return mixed
     */
    public function update(User $user, DigitasiJalan $digitasiJalan)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\DigitasiJalan  $digitasiJalan
     * @return mixed
     */
    public function delete(User $user, DigitasiJalan $digitasiJalan)
    {
        //
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User  $user
     * @param  \App\DigitasiJalan  $digitasiJalan
     * @return mixed
     */
    public function restore(User $user, DigitasiJalan $digitasiJalan)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\DigitasiJalan  $digitasiJalan
     * @return mixed
     */
    public function forceDelete(User $user, DigitasiJalan $digitasiJalan)
    {
        //
    }
}
