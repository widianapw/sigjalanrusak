<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eksisting extends Model
{
    protected $table = "eksisting_jalans";
    protected $fillable = ["eksisting"];

    public const VALIDATION_RULES = [
        "eksisting" => "required|min:3"
    ];
    
}
