<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widiana extends Model
{
    protected $table = "widianas";
    protected $fillable =["id","name"];

}
