<?php


namespace App\PushNotification\PengaduanStatus;

use App\PushNotification\FirebaseCloudMessaging;
use App\Traits\PengaduanStatusTrait;

class TerperbaikiNotification extends FirebaseCloudMessaging
{
    use PengaduanStatusTrait;

    protected function title(){
        return "Pengaduan sudah Diperbaiki";
    }

    protected function message()
    {

        return "Pengaduan anda dengan id ". $this->model->id." yang anda ajukan sudah diperbaiki";
    }
}
