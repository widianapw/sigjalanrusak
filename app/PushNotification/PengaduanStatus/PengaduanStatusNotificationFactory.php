<?php

namespace App\PushNotification\PengaduanStatus;
class PengaduanStatusNotificationFactory
{
    protected static $statusPengaduan = [
        0 => 'MenungguVerifikasiNotification',
        1 => 'TerverifikasiNotification',
        2 => 'TerperbaikiNotification',
        3 => 'SelesaiNotification',
        4 => 'DitolakNotification'
    ];

    public static function create($status)
	{
	    $providerClass = 'App\\PushNotification\\PengaduanStatus\\'.(static::$statusPengaduan[$status]?? static::$statusPengaduan[$status]);

	    if (class_exists($providerClass)) {
	    	// return new $providerClass($status);
	    	return new $providerClass;
	    }

	    abort(500, 'PushNotification provider not found');
	}
}