<?php


namespace App\PushNotification\PengaduanStatus;

use App\PushNotification\FirebaseCloudMessaging;
use App\Traits\PengaduanStatusTrait;

class DitolakNotification extends FirebaseCloudMessaging
{
    use PengaduanStatusTrait;

    protected function title(){
        return "Pengaduan Ditolak";
    }

    protected function message()
    {
        return "Pengaduan dengan id ". $this->model->id." yang anda ajukan sudah ditolak";
    }
}
