<?php


namespace App\PushNotification\PengaduanStatus;

use App\PushNotification\FirebaseCloudMessaging;
use App\Traits\PengaduanStatusTrait;

class TerverifikasiNotification extends FirebaseCloudMessaging
{
    use PengaduanStatusTrait;

    protected function title(){
        return "Pengaduan terverifikasi";
    }

    protected function message()
    {
        return "Pengaduan dengan id ". $this->model->id." yang anda ajukan sudah terverifikasi";
    }
}
