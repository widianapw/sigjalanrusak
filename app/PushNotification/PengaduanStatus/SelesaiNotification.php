<?php


namespace App\PushNotification\PengaduanStatus;

use App\PushNotification\FirebaseCloudMessaging;
use App\Traits\PengaduanStatusTrait;

class SelesaiNotification extends FirebaseCloudMessaging
{
    use PengaduanStatusTrait;

    protected function title(){
        return "Pengaduan Selesai";
    }

    protected function message()
    {
        return "Pengaduan dengan id ". $this->model->id." yang anda ajukan sudah selesai";
    }
}
