<?php


namespace App\PushNotification\PengaduanStatus;

use App\PushNotification\FirebaseCloudMessaging;
use App\Traits\PengaduanStatusTrait;

class MenungguVerifikasiNotification extends FirebaseCloudMessaging
{
    use PengaduanStatusTrait;

    protected function title(){
        return "Pengaduan Menunggu Verifikasi";
    }

    protected function message()
    {
        return "Pengaduan dengan id ". $this->model->id." yang anda ajukan sedang menunggu verifikasi dari pengelola jalan";
    }
}
