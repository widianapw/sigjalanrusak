<?php

namespace App\PushNotification;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\Log;

abstract class FirebaseCloudMessaging
{
    protected $model;
    protected $toAll;
    protected $baseUri = 'https://fcm.googleapis.com/fcm/';

    public static function send($model, $toAll = false)
    {
        return (new static())->request($model, $toAll);
    }

    public function request($model, $toAll)
    {
        $this->model = $model;
        $this->toAll = $toAll;

        $this->logs();

        RequestOptions::JSON;

        $client = $this->client();

        Log::info($this->content());

        return $client->request('POST', 'send', [
            'headers' => $this->headers(),
            'json' => $this->content()
        ]);
    }

    abstract protected function content();

    abstract protected function logs();

    protected function client()
    {
        return new Client([
            'base_uri' => $this->baseUri
        ]);
    }

    protected function headers()
    {
        return [
            'Authorization' => 'key=AAAA0ID8Fjk:APA91bHzsB-Jug0DfowreXKgsUt_buZJYcRQlwO-vqE84X9vwVyXNuc1iRKJgczF5s9qFuNwUrmtRJnWaZJdUfsQnlIrIR7XsZGubVt4Tme4XzCXZfhYZt8n7uet9vSV5H01VHCQVJF6',
        ];
    }
}
