<?php


namespace App\Traits;
use Auth;

trait PengaduanStatusTrait
{
    public function content()
    {
        return [
            // 'to'            => $this->determineTo(), //for global notif use topic
            // 'to'			=> $this->model->user->fcm_token,
            'to' => $this->model->pengguna->user->fcm_token,
            'notification'  => $this->notification(),
            'data' => $this->data()
        ];
    }

    protected function logs()
    {
        $user = $this->model->pengguna->user;
//
    }

    protected function notification()
    {
        return [
            'body'    => $this->message(),
            'title'   => $this->title(),
            'vibrate' => 1,
            'sound'   => 1,
        ];
    }

    protected function data()
    {
        return [
            'id'    => $this->model->id,
            'type'  => 'pengaduan',
            'title' => $this->title(),
            'body'  => $this->message(),
        ];
    }

    /**
     * send to topic (global push notif)
     * @format: platform-env-lang
     * ex: android-dev-en
     * */
    // protected function determineTo()
    // {
    // 	$to       = '/topics';
    // 	$platform = 'android'; //need to be dynamic for android and ios
    // 	$env      = env('APP_ENV');
    // 	$locale   = auth()->user()->lang;

    // 	return "{$to}/{$platform}-{$env}-{$locale}";
    // }
}
