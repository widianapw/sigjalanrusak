<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class JenisJalan extends Model
{
    use SoftDeletes;
    protected $table = "jenis_jalans";
    protected $primaryKey = "id";
    protected $guarded = [];
//    public $with=['digitasiJalan'];
    public function digitasiJalan(){
        return $this->hasMany(DigitasiJalan::class, 'id_jenis_jalan');
    }

    public const VALIDATION_RULES = [
        "jenis_jalan" => "required|min:3"
    ];
}
