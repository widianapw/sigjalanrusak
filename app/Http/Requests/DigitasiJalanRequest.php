<?php

namespace App\Http\Requests;

use App\DigitasiJalan;
use Illuminate\Foundation\Http\FormRequest;

class DigitasiJalanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return DigitasiJalan::VALIDATION_RULES;
    }
}
