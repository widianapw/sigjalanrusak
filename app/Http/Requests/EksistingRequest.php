<?php


namespace App\Http\Requests;

use App\Eksisting;
use Illuminate\Foundation\Http\FormRequest;

class EksistingRequest extends FormRequest{
    public function rules(){
        return Eksisting::VALIDATION_RULES;
    }
}
