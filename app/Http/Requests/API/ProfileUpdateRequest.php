<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
//            'email' => 'required|unique:users,email,{$this->user()->id}',
            'username' => ['required'],
            'phone' => ['required','numeric'],
        ];
    }

    public function authorize()
    {
        return $this->user();
    }
}
