<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class PengaduanRequest extends FormRequest
{
    public function rules()
    {
        return [
            'judul' => ['required'],
            'deskripsi' => ['required'],
            // 'foto_url' => ['required'],
            'latitude' => ['required'],
            'longitude' => ['required'],
            // 'id_digitasi_jalan' => ['required']
        ];
    }

    public function authorize()
    {
        return $this->user();
    }
}
