<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => ['required','unique:users'],
            'username' => ['required'],
            'phone' => ['required','numeric'],
            'password' => ['required']
        ];
    }

    public function authorize()
    {
        return true;
    }
}
