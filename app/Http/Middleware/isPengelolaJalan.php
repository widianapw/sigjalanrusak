<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\PengelolaJalan;
class isPengelolaJalan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pengelolaJalan = PengelolaJalan::where('id_user',Auth::id())->first();
        // return $pengelolaJalan;
        if(Auth::user() && Auth::user()->role == "1" && $pengelolaJalan->verified != NULL){
            return $next($request);
        }else if(Auth::user() && is_null($pengelolaJalan->verified)){
            return redirect('/verify-pengelola');
        }
        return redirect('/login');
    }
}
