<?php

namespace App\Http\Controllers\AuthAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAdmins;
use App\Provinsi;
use App\JenisJalan;
use App\Kabupaten;
use App\Desa;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Http\Request;
use App\Admin;
use Illuminate\Support\Facades\Hash;
class AdminAuthController extends Controller
{
    use AuthenticatesAdmins;

    protected $redirectTo = '/admin';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showRegisterForm(){
        $provinsi = Provinsi::get();
        $kabupaten = Kabupaten::select("kabupatens.id",DB::raw("CONCAT(kabupaten,' - ',provinsi) AS kabupaten"))
        ->join("provinsis","kabupatens.id_provinsi","=","provinsis.id")->get();
        $jenisJalan = JenisJalan::get();
        $desa = Desa::select("desas.id",DB::raw("CONCAT(desa,' - ',kabupaten) AS desa"))
        ->join("kecamatans","desas.id_kecamatan","=","kecamatans.id")
        ->join("kabupatens","kecamatans.id_kabupaten","=","kabupatens.id")
        ->get();
        return view("admin.register", compact("provinsi","kabupaten","jenisJalan","desa"));
    }
    
    public function register(Request $request){
        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = "2";
        $user->save();

        $admin = new Admin;
        $admin->id_user = $user->id;
        $admin->id_jenis_jalan = $request->id_jenis_jalan;
        if(!is_null($request->id_provinsi)){
            $admin->id_provinsi = $request->id_provinsi;
        }else if(!is_null($request->id_kabupaten)){
            $admin->id_kabupaten = $request->id_kabupaten;
        }else if(!is_null($request->id_desa)){
            $admin->id_desa = $request->id_desa;
        }
        
        $admin->save();
        $this->guard()->login($user);
        return view("admin.index");
    }
}
