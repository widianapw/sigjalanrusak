<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\PengaduanJalan;
use App\PengelolaJalan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminPengaduanJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PengaduanJalan::whereHas('digitasiJalan', function ($query) {
            $admin = Auth::user()->admin;
            $jenis_jalan = $admin->jenisJalan->jenis_jalan;
            if (strtolower($jenis_jalan) == "kota") {
                $jenis_jalan = "kabupaten";
            }
            $admin_identifier = "id_" . $jenis_jalan;
            // return $admin_identifier;

            $pengelolaJalan = PengelolaJalan::where($admin_identifier, $admin->$admin_identifier)->first();
            // $pengelolaJalan = $pengelolaJalan1->where('id_provinsi', $admin->id_provinsi)
            //     ->orWhere('id_kabupaten', $admin->id_kabupaten)
            //     ->orWhere('id_desa', $admin->id_desa)
            //     ->first();
            // $pengelolaJalan = $pengelolaJalan->filter(function ($pengelolaJalan) use ($searching_for) {
            //     return strstr($pengelolaJalan->field1, $searching_for) ||
            //         strstr($pengelolaJalan->field2, $searching_for) ||
            //         strstr($pengelolaJalan->field3, $searching_for) ||
            //         strstr($pengelolaJalan->field4, $searching_for);
            // });
            return $query->where('id_pengelola_jalan', '=', $pengelolaJalan->id);
        })->orderBy('id', 'desc')->get();
        // return $data;
        return view("admin.pengaduan_jalan.index", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PengaduanJalan $pengaduanJalan)
    {
        $data = PengaduanJalan::find($pengaduanJalan->id);
        return view("pengelola_jalan.pengaduan_jalan.detail", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
