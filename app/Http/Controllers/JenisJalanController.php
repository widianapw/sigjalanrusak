<?php

namespace App\Http\Controllers;

use App\Http\Requests\JenisJalanRequest;
use App\JenisJalan;
use Illuminate\Http\Request;

class JenisJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
       $data = JenisJalan::get();
       return view('superadmin.jenis_jalan.index', compact("data")); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('superadmin.jenis_jalan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JenisJalanRequest $request)
    {
        JenisJalan::insert($request->except(['_token']));
        return redirect('/superadmin/jenis-jalan')->with('success', $this->SUCCESS_ADD_MESSAGE);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JenisJalan  $jenisJalan
     * @return \Illuminate\Http\Response
     */
    public function show(JenisJalan $jenisJalan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JenisJalan  $jenisJalan
     * @return \Illuminate\Http\Response
     */
    public function edit(JenisJalan $jenisJalan)
    {
        return view("superadmin.jenis_jalan.edit", compact("jenisJalan"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JenisJalan  $jenisJalan
     * @return \Illuminate\Http\Response
     */
    public function update(JenisJalanRequest $request, JenisJalan $jenisJalan)
    {
        JenisJalan::find($jenisJalan->id)->update($request->all());
        return redirect('/superadmin/jenis-jalan')->with('success', $this->SUCCESS_UPDATE_MESSAGE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JenisJalan  $jenisJalan
     * @return \Illuminate\Http\Response
     */
    public function destroy(JenisJalan $jenisJalan)
    {
        JenisJalan::find($jenisJalan->id)->delete();
        return redirect('/superadmin/jenis-jalan')->with('success', $this->SUCCESS_DELETE_MESSAGE);
    }
}
