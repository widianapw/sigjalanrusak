<?php

namespace App\Http\Controllers;

use App\KoordinatDigitasiJalan;
use Illuminate\Http\Request;

class KoordinatDigitasiJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
         $this->middleware('auth:web');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KoordinatDigitasiJalan  $koordinatDigitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function show(KoordinatDigitasiJalan $koordinatDigitasiJalan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KoordinatDigitasiJalan  $koordinatDigitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function edit(KoordinatDigitasiJalan $koordinatDigitasiJalan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KoordinatDigitasiJalan  $koordinatDigitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KoordinatDigitasiJalan $koordinatDigitasiJalan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KoordinatDigitasiJalan  $koordinatDigitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function destroy(KoordinatDigitasiJalan $koordinatDigitasiJalan)
    {
        //
    }
}
