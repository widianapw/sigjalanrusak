<?php

namespace App\Http\Controllers;

use Auth;
use App\PengaduanImage;
use App\PengaduanJalan;
use Illuminate\Http\Request;

class PengaduanJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = PengaduanJalan::where('status', '0')->whereHas('digitasiJalan', function ($query) {
            return $query->where('id_pengelola_jalan', '=', Auth::user()->pengelolaJalan->id);
        })->orderBy('id', 'desc')->get();
        $dataTerverifikasi = PengaduanJalan::where('status', '1')->whereHas('digitasiJalan', function ($query) {
            return $query->where('id_pengelola_jalan', '=', Auth::user()->pengelolaJalan->id);
        })->orderBy('id', 'desc')->get();
        $dataSudahDiperbaiki = PengaduanJalan::where('status', '2')->whereHas('digitasiJalan', function ($query) {
            return $query->where('id_pengelola_jalan', '=', Auth::user()->pengelolaJalan->id);
        })->orderBy('id', 'desc')->get();
        $dataDitolak = PengaduanJalan::where('status', '3')->whereHas('digitasiJalan', function ($query) {
            return $query->where('id_pengelola_jalan', '=', Auth::user()->pengelolaJalan->id);
        })->orderBy('id', 'desc')->get();
        // return $data;
        return view("pengelola_jalan.pengaduan_jalan.index", compact("data", "dataTerverifikasi", "dataSudahDiperbaiki", "dataDitolak"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $request['id_bengkel'] = Auth::user()->id_bengkel;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengaduanJalan  $pengaduanJalan
     * @return \Illuminate\Http\Response
     */
    public function show(PengaduanJalan $pengaduanJalan)
    {
        $data = PengaduanJalan::find($pengaduanJalan->id);
        return view("pengelola_jalan.pengaduan_jalan.detail", compact("data"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengaduanJalan  $pengaduanJalan
     * @return \Illuminate\Http\Response
     */
    public function edit(PengaduanJalan $pengaduanJalan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengaduanJalan  $pengaduanJalan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengaduanJalan $pengaduanJalan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengaduanJalan  $pengaduanJalan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengaduanJalan $pengaduanJalan)
    {
        //
    }

    public function changeStatus(Request $request, $id)
    {
        $jalan = PengaduanJalan::find($id);
        $jalan->status = $request->status;
        $jalan->update();
        return redirect()->back()->with("success", "Status Pengaduan Berhasil Diubah!");
    }

    public function addTerperbaikiImages(Request $request)
    {
        $jalan = PengaduanJalan::find($request->id_pengaduan);
        $jalan->status = "2";
        $jalan->update();
        if ($request->hasfile('foto_terperbaiki')) {
            foreach ($request->file('foto_terperbaiki') as $file) {
                $name = uniqid() . '.png';
                $file->move(public_path() . "/img/jalan-rusak/", $name);
                $hasil[] = $name;
                $file = new PengaduanImage();
                $file->id_pengaduan = $request->id_pengaduan;
                $file->type = "terperbaiki";
                $file->foto_url = $name;
                $file->save();
            }
        }
        return redirect()->back()->with("success", "Status Pengaduan Berhasil Diubah!");
    }
}
