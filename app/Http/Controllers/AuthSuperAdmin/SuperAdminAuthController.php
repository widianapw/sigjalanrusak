<?php

namespace App\Http\Controllers\AuthSuperAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesSuperAdmins;

class SuperAdminAuthController extends Controller
{
    use AuthenticatesSuperAdmins;

    protected $redirectTo = '/superadmin';

    public function __construct()
    {
        $this->middleware('guest:superadmin')->except('logout');
    }
}
