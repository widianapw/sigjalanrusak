<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DigitasiJalan;
use App\Http\Resources\DigitasiResourceCollection;
use App\Http\Resources\DigitasiResource;

class DigitasiJalanController extends Controller
{
    public function index(){
        $digitasiJalan = DigitasiJalan::where('status','1')->get();
        // return $digitasiJalan;
        return new DigitasiResourceCollection($digitasiJalan);
    }
}
