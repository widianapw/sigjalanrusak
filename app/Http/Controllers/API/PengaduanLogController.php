<?php

namespace App\Http\Controllers\API;

use App\PengaduanLog;
use App\Http\Controllers\Controller;
use App\Http\Resources\PengaduanLogResourceCollection;
use Illuminate\Http\Request;
use App\Pengguna;
use Auth;
class PengaduanLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengguna = Pengguna::where('id_user',Auth::id())->first();
        $data = PengaduanLog::orderBy('id','desc')->where('id_pengguna',$pengguna->id)->get();
        // return $data;
        return new PengaduanLogResourceCollection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengaduanLog  $pengaduanLog
     * @return \Illuminate\Http\Response
     */
    public function show(PengaduanLog $pengaduanLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengaduanLog  $pengaduanLog
     * @return \Illuminate\Http\Response
     */
    public function edit(PengaduanLog $pengaduanLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengaduanLog  $pengaduanLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengaduanLog $pengaduanLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengaduanLog  $pengaduanLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengaduanLog $pengaduanLog)
    {
        //
    }
}
