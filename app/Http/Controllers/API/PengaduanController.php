<?php

namespace App\Http\Controllers\API;

use App\Pengguna;
use App\PengaduanImage;
use App\PengaduanJalan;
use Illuminate\Http\Request;
use App\Http\Resources\Success;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\PengaduanResource;
use App\Http\Requests\API\PengaduanRequest;
use App\Http\Resources\PengaduanResourceCollection;

class PengaduanController extends Controller
{

    public function getPengaduan()
    {
        $pengaduan = PengaduanJalan::orderBy('id', 'desc')->get();
        // return $pengaduan;
        return new PengaduanResourceCollection($pengaduan);
    }

    public function getPengaduanTerverifikasi()
    {
        $pengaduan = PengaduanJalan::orderBy('id', 'desc')->where('status', '1')->get();
        return new PengaduanResourceCollection($pengaduan);
    }

    public function postPengaduan(PengaduanRequest $request)
    {
        $request['status'] = "0";
        $pengguna = Pengguna::where('id_user', Auth::id())->first();
        $request['id_pengguna'] = $pengguna->id;
        PengaduanJalan::insert($request->except(['_token']));
        $pengaduan = PengaduanJalan::where('id_pengguna', $pengguna->id)->orderBy('id', 'desc')->first();
        return new PengaduanResource($pengaduan);
    }

    public function uploadImagePengaduan(Request $request, $id)
    {
        $pengaduan = PengaduanJalan::where('id', $id)->first();
        // $pengaduan = PengaduanJalan::find($id)->first();
        // $pengaduan->foto_url = $fullImageName;
        // $pengaduan->save();
        foreach ($request->file('image') as $image) {
            $name = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path() . '/img/jalan-rusak/', $name);
            $fullImageName = $name;
            $insert = new PengaduanImage;
            $insert->id_pengaduan = $pengaduan->id;
            $insert->type = "pengaduan";
            $insert->foto_url = $fullImageName;
            $insert->save();
            // PengaduanImage::create([
            //     'id_pengaduan' => $pengaduan->id,
            //     'foto_url' => $fullImageName
            // ]);
        }

        return new PengaduanResource($pengaduan);
    }

    public function changeStatus(PengaduanJalan $pengaduanJalan, Request $request)
    {
        $pengaduanJalan->status = $request->status;
        $pengaduanJalan->save();
        return new Success([]);
    }

    public function getPengaduanDetail($id)
    {
        $pengaduan = PengaduanJalan::where('id', $id)->first();
        return new PengaduanResource($pengaduan);
    }

    public function getPengaduanSearch($key)
    {
        $pengaduan = PengaduanJalan::where('judul', 'like', '%' . $key . '%')
            ->orWhere('deskripsi', 'like', '%' . $key . '%')
            ->orWhere('alamat', 'like', '%' . $key . '%')
            ->orderBy('id', 'desc')->get();
        return new PengaduanResourceCollection($pengaduan);
    }
}
