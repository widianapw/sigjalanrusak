<?php

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;

use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\ProfileUpdateRequest;
use App\Http\Requests\API\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Pengguna;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->where('role', '3')->first();
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password]) || empty($user)) {
            return response()->json([
                'error' => [
                    'code' => 422,
                    'title' => 'Error',
                    'errors' => [
                        [
                            'title' => 'Data not Matched',
                            'message' => 'Wrong email and Password'
                        ]
                    ]
                ]
            ], 422);
        } else {
            $accessToken = Auth::user()->createToken('authToken')->accessToken;
            $pengguna = Pengguna::whereHas('user', function ($q) {
                return $q->where('id', '=', Auth::user()->id);
            })->first();
            $pengguna->accessToken = $accessToken;
            $user->fcm_token = $request->fcm_token;
            $user->save();

            return new UserResource($pengguna);
        }
    }

    public function register(RegisterRequest $request)
    {
        $user = new User;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role = "3";
        $user->fcm_token = $request->fcm_token;
        $user->password = bcrypt($request->password);
        $user->save();
        $pengguna = new Pengguna;
        $pengguna->id_user = $user->id;
        $pengguna->phone = $request->phone;
        $pengguna->save();
        $data = Pengguna::find($pengguna->id);
        $data->accessToken = $user->createToken('authToken')->accessToken;
        return new UserResource($data);
    }

    public function getProfile()
    {
        $pengguna = Pengguna::where('id_user', Auth::id())->first();
        $data = Pengguna::find($pengguna->id);
        return new UserResource($data);
    }

    public function updateProfile(ProfileUpdateRequest $request)
    {
        $user = Auth::user();
        $user->username = $request->username;
        $user->save();
        $pengguna = Pengguna::where('id_user', $user->id)->first();
        $pengguna->phone = $request->phone;
        // $pengguna->photo_url = $request->photoUrl;
        $pengguna->save();
        $data = Pengguna::find($pengguna->id);
        return new UserResource($data);
    }

    public function updateFcmToken(Request $request)
    {
        $user = Auth::user();
        $user->fcm_token = $request->fcm_token;
        $user->save();
        $pengguna = Pengguna::where('id_user', Auth::id())->first();
        $data = Pengguna::find($pengguna->id);
        return new UserResource($data);
    }

    public function updateProfileImage(Request $request)
    {
        $image = $request->file('image');

        $name = time() . '_' . $image->getClientOriginalName();
        $image->move(public_path() . '/img/profile/', $name);
        $fullImageName = $name;
        $pengguna = Pengguna::where('id_user', Auth::id())->first();
        $pengguna->photo_url = $fullImageName;
        $pengguna->save();
        return new UserResource($pengguna);
    }
}
