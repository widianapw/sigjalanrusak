<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\Success;
use App\Http\Resources\UserResource;
use App\Pengguna;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class NuxtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Pengguna::latest()->get();
        return UserResource::collection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $request->password = Hash::make($request->password);
        $user = User::updateOrCreate($request->except(['_token']));
        $pengguna = Pengguna::create(['id_user' => $user->id]);
        return new UserResource($pengguna);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Pengguna::find($id)->first();
        return new UserResource($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {

        if ($request->has('password')) {
            $request->password = Hash::make($request->password);
        }
        $userId = Pengguna::find($id)->first()->user->id;
        User::find($userId)->update($request->all());
        $pengguna = Pengguna::where('id_user', $userId)->first();
        return new UserResource($pengguna);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pengguna::find($id)->delete();
        // $pengguna->delete();
        return new Success([]);
    }
}
