<?php

namespace App\Http\Controllers;

use App\Notifications\NewPengaduan;
use App\PengaduanJalan;
use Telegram\Bot\Laravel\Facades\Telegram;

class ChatController extends Controller
{
   public function index()
   {
      //   return view("chat.index");
   }

   public function subdomain($username)
   {
      return $username;
   }

   public function telegeramBot()
   {

      // $message = Telegram::sendMessage([
      //    'chat_id' => '449645516',
      //    'text' => 'Ada Pesanan Baru'
      // ]);


      // $messageId = $response->getMessageId();
      // return $messageId;
      // $response = Telegram::getUpdates();
      // return $response;
      // $response = Telegram::getMe();

      // $botId = $response->getId();
      // $firstName = $response->getFirstName();
      // $username = $response->getUsername();
      // return $response;
      $pengaduan = PengaduanJalan::first();
      $pengaduan->notify(new NewPengaduan($pengaduan));
      return $pengaduan;
   }
}
