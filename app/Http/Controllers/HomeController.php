<?php

namespace App\Http\Controllers;

use App\PengaduanJalan;
use App\PengelolaJalan;
use Illuminate\Http\Request;
use Auth;

use Illuminate\Support\Facades\DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $baliPertahun = Tanaman::select(DB::raw('COALESCE (SUM(produksi),0) as total_produksi'),'jenis_tanamans.jenis_tanaman','tahuns.tahun','produksi')
        // ->join('jenis_tanamans','tanamans.jenis_tanaman_id','=','jenis_tanamans.id')
        // ->join('tahuns','tanamans.tahun_id','=','tahuns.id')
        // ->groupBy('jenis_tanamans.jenis_tanaman')
        // ->get();
        $data = PengaduanJalan::whereHas('digitasiJalan', function($query){
            return $query->where('id_pengelola_jalan','=',Auth::user()->pengelolaJalan->id);
        })->get();
        $dataMenunggu = PengaduanJalan::where('status','0')->whereHas('digitasiJalan', function($query){
            return $query->where('id_pengelola_jalan','=',Auth::user()->pengelolaJalan->id);
       })->get();
        $dataTerverifikasi = PengaduanJalan::where('status','1')->whereHas('digitasiJalan', function($query){
            return $query->where('id_pengelola_jalan','=',Auth::user()->pengelolaJalan->id);
        })->get();
        $dataSudahDiperbaiki = PengaduanJalan::where('status','2')->whereHas('digitasiJalan', function($query){
            return $query->where('id_pengelola_jalan','=',Auth::user()->pengelolaJalan->id);
        })->get();
        $dataDitolak = PengaduanJalan::where('status','3')->whereHas('digitasiJalan', function($query){
            return $query->where('id_pengelola_jalan','=',Auth::user()->pengelolaJalan->id);
        })->get();
        
        $totalPengaduan = count($data);
        $totalMenunggu = count($dataMenunggu);
        $totalTerverifikasi = count($dataTerverifikasi);
        $totalSudahDiperbaiki = count($dataSudahDiperbaiki);
        $totalDitolak = count($dataDitolak);
        return view('dashboard', compact("totalPengaduan","totalMenunggu","totalTerverifikasi","totalSudahDiperbaiki","totalDitolak"));
    }

    public function verifyPengelola(){
        $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_user',Auth::id())->get()->first();
        return view('pengelola_jalan.verify', compact("data"));
    }
}
