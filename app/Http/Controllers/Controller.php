<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{

    
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $SUCCESS_ADD_MESSAGE = "Data berhasil ditambahkan";
    public $SUCCESS_UPDATE_MESSAGE = "Data berhasil diubah";
    public $SUCCESS_DELETE_MESSAGE = "Data berhasil dihapus";

    public $BASE_URL = "https://ff6bce272a50.ngrok.io";
    public $IMAGE_PENGADUAN_PATH  = 'https://ff6bce272a50.ngrok.io/img/jalan-rusak/';
    public $IMAGE_PROFILE_PATH  = 'https://ff6bce272a50.ngrok.io/img/profile/';

}
