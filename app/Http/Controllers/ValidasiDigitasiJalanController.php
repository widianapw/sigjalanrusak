<?php

namespace App\Http\Controllers;

use App\Admin;
use App\PengelolaJalan;
use Illuminate\Http\Request;
use App\DigitasiJalan;
use App\JenisJalan;
use Auth;

class ValidasiDigitasiJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        //        $this->authorize("index", PengelolaJalan::class);
        //
        //
        //        if($admin->id_jenis_jalan == "1" ){
        //            $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_jenis_jalan',$admin->id_jenis_jalan)->where('id_provinsi',$admin->id_provinsi)->get();
        //        }else if($admin->id_jenis_jalan == "3"|| $admin->id_jenis_jalan == "2" ){
        //            $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_jenis_jalan',$admin->id_jenis_jalan)->where('id_kabupaten',$admin->id_kabupaten)->get();
        //        }else if($admin->id_jenis_jalan == "4"){
        //            $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_jenis_jalan',$admin->id_jenis_jalan)->where('id_desa',$admin->id_desa)->get();
        //        }
        //        $data = DigitasiJalan::
        $admin = Admin::where('id_user', Auth::id())->first();
        if ($admin->id_jenis_jalan == "1") {
            $id = "id_provinsi";
            $identifier = $admin->id_provinsi;
        } else if ($admin->id_jenis_jalan == "3" || $admin->id_jenis_jalan == "2") {
            $id = "id_kabupaten";
            $identifier = $admin->id_kabupaten;
        } else if ($admin->id_jenis_jalan == "4") {
            $id = "id_desa";
            $identifier = $admin->id_desa;
        }
        $data = DigitasiJalan::select('digitasi_jalans.id', 'nama_jalan', 'ruas_jalan', 'digitasi_jalans.created_at', 'users.username', 'digitasi_jalans.status')
            ->join('pengelola_jalans', 'digitasi_jalans.id_pengelola_jalan', '=', 'pengelola_jalans.id')
            ->join('users', 'pengelola_jalans.id_user', '=', 'users.id')
            ->where('pengelola_jalans.' . $id, $identifier)->orderBy('digitasi_jalans.id', 'desc')->get();

        //        $data = DigitasiJalan::whereHas('pengelolaJalan', function ($query) {
        ////
        //
        ////            return $query->where('pengelola_jalan.id_kabupaten', $admin->id_jenis_jalan);
        //        })->orderBy('id', 'desc')->get();
        return view("admin.validasi_digitasi_jalan.index", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DigitasiJalan::find($id);
        return view("admin.validasi_digitasi_jalan.detail", compact("data"));
    }

    public function detailJalan($id)
    {
        $data = DigitasiJalan::with('jenisJalan', 'pengelolaJalan', 'detailDigitasi')->find($id);
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verifDigitasi($id)
    {
        $jalan = DigitasiJalan::find($id);
        $jalan->status = "1";
        $jalan->update();
        return redirect('/admin/validasi-digitasi-jalan')->with("success", "Berhasil Diperbaharui!");
    }

    public function tolakDigitasi($id)
    {
        $jalan = DigitasiJalan::find($id);
        $jalan->status = "2";
        $jalan->update();
        return redirect('/admin/validasi-digitasi-jalan')->with("success", "Berhasil Diperbaharui!");
    }
}
