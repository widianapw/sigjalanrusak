<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\DigitasiJalan;
use App\Admin;
use App\PengaduanJalan;
use App\PengelolaJalan;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        $admin = Admin::where('id_user', Auth::id())->first();
        if ($admin->id_jenis_jalan == "1") {
            $id = "id_provinsi";
            $identifier = $admin->id_provinsi;
        } else if ($admin->id_jenis_jalan == "3" || $admin->id_jenis_jalan == "2") {
            $id = "id_kabupaten";
            $identifier = $admin->id_kabupaten;
        } else if ($admin->id_jenis_jalan == "4") {
            $id = "id_desa";
            $identifier = $admin->id_desa;
        }
        $data = DigitasiJalan::select('digitasi_jalans.id', 'nama_jalan', 'ruas_jalan', 'digitasi_jalans.created_at', 'users.username', 'digitasi_jalans.status')
            ->join('pengelola_jalans', 'digitasi_jalans.id_pengelola_jalan', '=', 'pengelola_jalans.id')
            ->join('users', 'pengelola_jalans.id_user', '=', 'users.id')
            ->where('pengelola_jalans.' . $id, $identifier)->get();
        $dataMenungguVerifikasi = DigitasiJalan::select('digitasi_jalans.id', 'nama_jalan', 'ruas_jalan', 'digitasi_jalans.created_at', 'users.username', 'digitasi_jalans.status')
            ->join('pengelola_jalans', 'digitasi_jalans.id_pengelola_jalan', '=', 'pengelola_jalans.id')
            ->join('users', 'pengelola_jalans.id_user', '=', 'users.id')
            ->where('pengelola_jalans.' . $id, $identifier)->where('status', '0')->get();
        $dataTerverifikasi = DigitasiJalan::select('digitasi_jalans.id', 'nama_jalan', 'ruas_jalan', 'digitasi_jalans.created_at', 'users.username', 'digitasi_jalans.status')
            ->join('pengelola_jalans', 'digitasi_jalans.id_pengelola_jalan', '=', 'pengelola_jalans.id')
            ->join('users', 'pengelola_jalans.id_user', '=', 'users.id')
            ->where('pengelola_jalans.' . $id, $identifier)->where('status', '1')->get();
        $dataDitolak = DigitasiJalan::select('digitasi_jalans.id', 'nama_jalan', 'ruas_jalan', 'digitasi_jalans.created_at', 'users.username', 'digitasi_jalans.status')
            ->join('pengelola_jalans', 'digitasi_jalans.id_pengelola_jalan', '=', 'pengelola_jalans.id')
            ->join('users', 'pengelola_jalans.id_user', '=', 'users.id')
            ->where('pengelola_jalans.' . $id, $identifier)->where('status', '2')->get();

        $total = count($data);
        $totalMenungguVerifikasi = count($dataMenungguVerifikasi);
        $totalTerverifikasi = count($dataTerverifikasi);
        $totalDitolak = count($dataDitolak);


        //data pengaduan
        $dataPengaduan = PengaduanJalan::whereHas('digitasiJalan', function ($query) {
            $admin = Auth::user()->admin;
            $jenis_jalan = $admin->jenisJalan->jenis_jalan;
            if (strtolower($jenis_jalan) == "kota") {
                $jenis_jalan = "kabupaten";
            }
            $admin_identifier = "id_" . $jenis_jalan;
            $pengelolaJalan = PengelolaJalan::where($admin_identifier, $admin->$admin_identifier)->first();
            return $query->where('id_pengelola_jalan', '=', $pengelolaJalan->id);
        })->get();

        $dataPengaduanMenunggu = PengaduanJalan::where('status', '0')->whereHas('digitasiJalan', function ($query) {
            $admin = Auth::user()->admin;
            $jenis_jalan = $admin->jenisJalan->jenis_jalan;
            if (strtolower($jenis_jalan) == "kota") {
                $jenis_jalan = "kabupaten";
            }
            $admin_identifier = "id_" . $jenis_jalan;
            $pengelolaJalan = PengelolaJalan::where($admin_identifier, $admin->$admin_identifier)->first();
            return $query->where('id_pengelola_jalan', '=', $pengelolaJalan->id);
        })->get();

        $dataPengaduanTerverifikasi = PengaduanJalan::where('status', '1')->whereHas('digitasiJalan', function ($query) {
            $admin = Auth::user()->admin;
            $jenis_jalan = $admin->jenisJalan->jenis_jalan;
            if (strtolower($jenis_jalan) == "kota") {
                $jenis_jalan = "kabupaten";
            }
            $admin_identifier = "id_" . $jenis_jalan;
            $pengelolaJalan = PengelolaJalan::where($admin_identifier, $admin->$admin_identifier)->first();
            return $query->where('id_pengelola_jalan', '=', $pengelolaJalan->id);
        })->get();

        $dataPengaduanSudahDiperbaiki = PengaduanJalan::where('status', '2')->whereHas('digitasiJalan', function ($query) {
            $admin = Auth::user()->admin;
            $jenis_jalan = $admin->jenisJalan->jenis_jalan;
            if (strtolower($jenis_jalan) == "kota") {
                $jenis_jalan = "kabupaten";
            }
            $admin_identifier = "id_" . $jenis_jalan;
            $pengelolaJalan = PengelolaJalan::where($admin_identifier, $admin->$admin_identifier)->first();
            return $query->where('id_pengelola_jalan', '=', $pengelolaJalan->id);
        })->get();

        $dataPengaduanDitolak = PengaduanJalan::where('status', '3')->whereHas('digitasiJalan', function ($query) {
            $admin = Auth::user()->admin;
            $jenis_jalan = $admin->jenisJalan->jenis_jalan;
            if (strtolower($jenis_jalan) == "kota") {
                $jenis_jalan = "kabupaten";
            }
            $admin_identifier = "id_" . $jenis_jalan;
            $pengelolaJalan = PengelolaJalan::where($admin_identifier, $admin->$admin_identifier)->first();
            return $query->where('id_pengelola_jalan', '=', $pengelolaJalan->id);
        })->get();

        $totalPengaduan = count($dataPengaduan);
        $totalPengaduanMenunggu = count($dataPengaduanMenunggu);
        $totalPengaduanTerverifikasi = count($dataPengaduanTerverifikasi);
        $totalPengaduanSudahDiperbaiki = count($dataPengaduanSudahDiperbaiki);
        $totalPengaduanDitolak = count($dataPengaduanDitolak);


        return view('admin.index', compact(
            "total",
            "totalMenungguVerifikasi",
            "totalTerverifikasi",
            "totalDitolak",
            "totalPengaduan",
            "totalPengaduanMenunggu",
            "totalPengaduanTerverifikasi",
            "totalPengaduanSudahDiperbaiki",
            "totalPengaduanDitolak"
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
