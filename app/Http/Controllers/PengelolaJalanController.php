<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Admin;
use Carbon\Carbon;

use App\PengelolaJalan;
use Illuminate\Http\Request;

class PengelolaJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = Admin::where('id_user',Auth::id())->first();
        if($admin->id_jenis_jalan == "1" ){
            $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_jenis_jalan',$admin->id_jenis_jalan)->where('id_provinsi',$admin->id_provinsi)->get();     
        }else if($admin->id_jenis_jalan == "3"|| $admin->id_jenis_jalan == "2" ){
            $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_jenis_jalan',$admin->id_jenis_jalan)->where('id_kabupaten',$admin->id_kabupaten)->get();     
        }else if($admin->id_jenis_jalan == "4"){
            $data = PengelolaJalan::with('user','provinsi','kabupaten','desa','jenisJalan')->where('id_jenis_jalan',$admin->id_jenis_jalan)->where('id_desa',$admin->id_desa)->get();     
        }
        return view("admin.pengelola_jalan.index",compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PengelolaJalan  $pengelolaJalan
     * @return \Illuminate\Http\Response
     */
    public function show(PengelolaJalan $pengelolaJalan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PengelolaJalan  $pengelolaJalan
     * @return \Illuminate\Http\Response
     */
    public function edit(PengelolaJalan $pengelolaJalan)
    {
        return view("admin.pengelola_jalan.edit");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PengelolaJalan  $pengelolaJalan
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, PengelolaJalan $pengelolaJalan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PengelolaJalan  $pengelolaJalan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengelolaJalan $pengelolaJalan)
    {
        //
    }

    public function changeStatus(PengelolaJalan $pengelolaJalan, Request $request){
        $user = PengelolaJalan::find($pengelolaJalan->id);
        $user->verified = Carbon::now();
        $user->update();
        return \redirect()->back()->with("success",$this->SUCCESS_UPDATE_MESSAGE);
    }
}
