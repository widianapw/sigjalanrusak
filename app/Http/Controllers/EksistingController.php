<?php

namespace App\Http\Controllers;

use App\Eksisting;
use App\Http\Controllers\Controller;
use App\Http\Requests\EksistingRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;

class EksistingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     */
    public function index()
    {
        $data = Eksisting::get();
        return view('superadmin.eksisting.index', compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|Response|View
     */
    public function create()
    {
        return view('superadmin.eksisting.create');
    }

    public function store(EksistingRequest $request)
    {
        Eksisting::insert($request->except(['_token']));
        return redirect('/superadmin/eksisting')->with('success','Data berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Eksisting  $eksisting
     * @return Response
     */
    public function show(Eksisting $eksisting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Eksisting  $eksisting
     * @return Response
     */
    public function edit(Eksisting $eksisting)
    {
        return view('superadmin.eksisting.edit', compact("eksisting"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  \App\Eksisting  $eksisting
     * @return Response
     */
    public function update(EksistingRequest $request, Eksisting $eksisting)
    {
        Eksisting::find($eksisting->id)->update($request->all());
        return redirect('/superadmin/eksisting')->with('success','Data '.$eksisting->eksisting.' berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Eksisting  $eksistin
     * @return Response
     */
    public function destroy(Eksisting $eksisting)
    {
        Eksisting::find($eksisting->id)->delete();
        return redirect('/superadmin/eksisting')->with('success','Data berhasil dihapus');
    }
}
