<?php

namespace App\Http\Controllers;

use App\DigitasiJalan;
use App\JenisJalan;
use App\KoordinatDigitasiJalan;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Kabupaten;
use App\Desa;
use App\Eksisting;
use App\Http\Requests\DigitasiJalanRequest;
use App\PengelolaJalan;

class DigitasiJalanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $view = "pengelola_jalan.digitasi_jalan.";
    public function index()
    {
//        $digitasiJalan = DigitasiJalan::get();
//        $pengelolaJalan = UserResource::where('id',Auth::user()->id)->get();
        $data = DigitasiJalan::where('id_pengelola_jalan', Auth::user()->pengelolaJalan->id)->orderBy("id","desc")->get();
        // $dataTerverifikasi = DigitasiJalan::with('jenisJalan')->where('id_pengelola_jalan',Auth::user()->pengelolaJalan->id)->where('status','1')->orderBy('id','desc')->get();
        // $dataDitolak = DigitasiJalan::with('jenisJalan')->where('id_pengelola_jalan',Auth::id())->where('status','2')->orderBy('id','desc')->get();
        return view($this->view."index", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenisJalan = JenisJalan::get();
        $eksisting = Eksisting::get();
        $user = PengelolaJalan::with('jenisJalan','user','provinsi','kabupaten','desa')->whereHas('user', function($query){
            return $query->where('id', Auth::id());
        })->first();
        $jenis = strtolower($user->jenisJalan->jenis_jalan);
        $validasi = "";
        if($jenis == "kabupaten" || $jenis == "kota"){
            $validasi = Kabupaten::with('provinsi')->where('id',$user->id_kabupaten)->first();
        }if($jenis == "desa"){
            $validasi = Desa::with('kecamatan')->where('id',$user->id_desa)->first();
        }
        return view($this->view."create", compact("jenisJalan","eksisting","user","validasi"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DigitasiJalanRequest $request)
    {
        $pengelolaJalan = PengelolaJalan::where('id_user', Auth::id())->first();
        if($request->isEdit){
            $digitasiJalan = DigitasiJalan::where('id',$request->id_jalan)->first();
            $detail = KoordinatDigitasiJalan::where('id_digitasi_jalan',$request->id_jalan)->get();
            $detail->each->delete();
        }else{
            $digitasiJalan = new DigitasiJalan;
        }
        $digitasiJalan->id_pengelola_jalan = $pengelolaJalan->id;
        $digitasiJalan->nama_jalan = $request->nama_jalan;
        $digitasiJalan->ruas_jalan = $request->ruas_jalan;
        $digitasiJalan->panjang_ruas = $request->panjang_ruas;
        $digitasiJalan->lebar_ruas = $request->lebar_ruas;
        $digitasiJalan->id_jenis_jalan = $request->id_jenis_jalan;
        $digitasiJalan->id_eksisting = $request->id_eksisting;
        $digitasiJalan->save();
        foreach ($request->koordinat as $item) {
            $detailKoordinat = new KoordinatDigitasiJalan;
            $detailKoordinat->id_digitasi_jalan = $digitasiJalan->id;
            $detailKoordinat->latitude = $item['lat'];
            $detailKoordinat->longitude = $item['lng'];
            $detailKoordinat->save();
        }
        return response()->json(['success'=>'Got Simple Ajax Request.']);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DigitasiJalan  $digitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function show(DigitasiJalan $digitasiJalan)
    {

        return $digitasiJalan->detailDigitasi;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DigitasiJalan  $digitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function edit(DigitasiJalan $digitasiJalan)
    {
        // return $digitasiJalan;
        $jenisJalan = JenisJalan::get();
        $eksisting = Eksisting::get();
        $user = PengelolaJalan::with('jenisJalan','user','provinsi','kabupaten','desa')->whereHas('user', function($query){
            return $query->where('id', Auth::id());
        })->first();
        $jenis = strtolower($user->jenisJalan->jenis_jalan);
        $validasi = "";
        if($jenis == "kabupaten" || $jenis == "kota"){
            $validasi = Kabupaten::with('provinsi')->where('id',$user->id_kabupaten)->first();
        }if($jenis == "desa"){
            $validasi = Desa::with('kecamatan')->where('id',$user->id_desa)->first();
        }
        return view($this->view."create", compact("digitasiJalan","jenisJalan","eksisting","user","validasi"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DigitasiJalan  $digitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DigitasiJalan $digitasiJalan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DigitasiJalan  $digitasiJalan
     * @return \Illuminate\Http\Response
     */
    public function destroy(DigitasiJalan $digitasiJalan)
    {
        DigitasiJalan::find($digitasiJalan->id)->delete();
        return redirect('/digitasi-jalan')->with('success', $this->SUCCESS_DELETE_MESSAGE);
    }
}
