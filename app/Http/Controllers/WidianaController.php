<?php

namespace App\Http\Controllers;

use App\Widiana;
use Illuminate\Http\Request;

class WidianaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Widiana $widiana
     * @return \Illuminate\Http\Response
     */
    public function show(Widiana $widiana)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Widiana $widiana
     * @return \Illuminate\Http\Response
     */
    public function edit(Widiana $widiana)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Widiana $widiana
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Widiana $widiana)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Widiana $widiana
     * @return \Illuminate\Http\Response
     */
    public function destroy(Widiana $widiana)
    {
        //
    }
}
