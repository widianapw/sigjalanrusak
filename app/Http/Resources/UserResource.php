<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'accessToken' => $this->accessToken,
            'email' => $this->user->email,
            'username' => $this->user->username,
            'photoUrl' => url('/img/profile') . '/' . $this->photo_url,
            'phone' => $this->phone
        ];
    }
}
