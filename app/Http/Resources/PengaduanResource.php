<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PengaduanResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'judul' => $this->judul,
            'deskripsi' => $this->deskripsi,
            'status' => $this->status,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'tanggal' => $this->created_at,
            'username' => $this->pengguna->user->username,
            'userPhotoUrl' => url('/img/profile') . '/' .  $this->pengguna->photo_url,
            'pengaduanPhotoUrl' => url('/img/jalan-rusak/') . '/' . $this->foto_url,
            'images' => ImageResource::collection($this->images),
            'namaJalan' => $this->digitasiJalan->nama_jalan,
            'jenisJalan' => $this->digitasiJalan->jenisJalan->jenis_jalan,
            'pengelolaJalan' => $this->digitasiJalan->pengelolaJalan->user->username,
            'alamat' => $this->alamat
        ];
    }
}
