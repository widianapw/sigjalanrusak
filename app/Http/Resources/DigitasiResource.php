<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DigitasiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       return [
            'id' => $this->id,
            'namaJalan' => $this->nama_jalan,
            'ruasJalan' => $this->ruas_jalan,
            'eksistingJalan' => $this->eksistingJalan->eksisting,
            'koordinatDigitasi' => KoordinatDigitasi::collection($this->detailDigitasi)
        ];
    }
}
