<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/** @see \App\PengaduanJalan */
class PengaduanResourceCollection extends ResourceCollection
{
    public $collects = PengaduanResource::class;
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
